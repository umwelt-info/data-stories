### ### ### ###
import pandas as pd
import requests
from urllib.parse import urlparse
from pathlib import Path
import os

from fetch import fetch_dataframe
from lib.ui_logging import logging


def build_feature(dataset):
    """Builds a dataset with the necessary keys for downloading the resources contained.

    Resources will be expanded later.
    """
    data_source = dataset["source"]
    data_id = dataset["id"]
    try:
        data_resources = dataset["resources"]
    except KeyError:
        return
    open_file_format = (
        dataset.get("quality", {}).get("interoperability", {}).get("open_file_format")
    )

    properties = {
        "source": data_source,
        "id": data_id,
        "resources": data_resources,
        "open_file_format": open_file_format,
    }
    return properties


def execute_direct_download(dataset):
    """Takes a pd.Series containing information about a single resource and downloads into a subfolder.


    @params:
        dataset: pd.Series with the following entries:
            direct_link: bool
            source: str
            id: str
            url: str

    @returns: None
    """
    if not dataset["direct_link"]:
        logging.error(f"No direct link found on {dataset['id']}")
        return
    url = dataset["url"]
    parsed_url = urlparse(url)
    base_name = os.path.basename(parsed_url.path)
    directory = Path("./downloads").joinpath(dataset["source"]).joinpath(dataset["id"])
    directory.mkdir(parents=True, exist_ok=True)
    full_path = directory.joinpath(base_name)

    if full_path.is_file():
        logging.info(f"{full_path} already found.")
        return

    logging.debug(f"Initialize download:\n\t{url}\ninto\n\t{full_path}")
    response = requests.get(url)

    if response.status_code == 200:
        with open(full_path, "wb") as file:
            file.write(response.content)
        logging.info(f"File downloaded successfully into {full_path}")
    else:
        logging.error(
            f"Failed to download the file. Status code: {response.status_code}"
        )


def get_confirmation(prompt="Do you want to proceed? (y/n): "):
    while True:
        response = input(prompt).strip().lower()
        if response in ["y", "yes"]:
            return True
        elif response in ["n", "no"]:
            return False
        else:
            print("Invalid input. Please enter 'y' or 'n'.")


if __name__ == "__main__":
    logging.getLogger().setLevel("DEBUG")
    try:  # Just to avoid polling the datasets multiple times when there already in the namespace.
        datasets
    except NameError:
        print("Fetch datasets")
        datasets = fetch_dataframe(
            query="type:/Messwerte/Grundwasser/Messstellen",
            build_row=build_feature,
            filter_datasets=lambda dataset: "resources" in dataset,
        )
    resources = datasets.explode(column="resources").reset_index(drop=False)
    resources = pd.concat((resources, pd.json_normalize(resources.resources)), axis=1)

    resources = resources.query("`type.label` in ['CSV', 'ZIP']")
    resources = resources.drop_duplicates(subset="url")

    if get_confirmation(
        f"This will download {len(resources)} files.\nDo you want to proceed? [Y/n]"
    ):
        resources.apply(execute_direct_download, axis=1)
    else:
        logging.info("Aborted")
