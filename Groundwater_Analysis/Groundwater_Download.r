###################################################################################
### Install Packages
###################################################################################

### ### ### ###
packages_list <- c("jsonlite", "leaflet", "leaflet.extras", "geojsonsf", "tidyverse", "sf")
new_packages <- packages_list[!(packages_list %in% installed.packages()[,"Package"])]
if(length(new_packages)) install.packages(new_packages)

### ### ### ###
library(jsonlite);packageVersion("jsonlite")
library(leaflet);packageVersion("leaflet")
library(leaflet.extras);packageVersion("leaflet.extras")
library(geojsonsf);packageVersion("geojsonsf")
library(tidyverse);packageVersion("tidyverse")
library(sf);packageVersion("sf")

###########################################################
# Define global options ----------------------------------
###########################################################

### ### ### ###
options(timeout=3000)
options(width = 150)

###########################################################
# Get data -----------------------------------------
###########################################################

### ### ### ### get datasets for groundwater wells
dataframe <- stream_in(url("https://md.umwelt.info/search/all?query=type:/Messwerte/Grundwasser/Messstellen"), pagesize = 10000)

###########################################################
# Filter for data -----------------------------------------
###########################################################

### ### ### ### extract relevant data 
data <-
  dataframe |> 
  as_tibble() |>
  select(c(source, id, resources)) |>
  unnest(col = c("resources")) |>
  unnest(col = c("type")) |>
  filter(label %in% c("CSV", "ZIP")) |>
  distinct(url, .keep_all = TRUE)

###########################################################
# Download data  ----------------------------------
###########################################################
dir.create("Groundwater_Data")
setwd("Groundwater_Data")
urls <- data$url
### ### ### ###
tryCatch({
  # Attempt to download the file
  for (url in urls) {
    download.file(url, destfile = basename(url))
}
  cat("File downloaded successfully and saved as", destfile, "\n")
}, error = function(e) {
  # Handle the error: print a custom error message
  cat("Error downloading the file:", e$message, "\n")
})
