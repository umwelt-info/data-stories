###################################################################################
### Load Packages
###################################################################################

### ### ### ###
library(jsonlite);packageVersion("jsonlite")
library(leaflet);packageVersion("leaflet")
library(leaflet.extras);packageVersion("leaflet.extras")
library(geojsonsf);packageVersion("geojsonsf")
library(tidyverse);packageVersion("tidyverse")
library(sf);packageVersion("sf")

###########################################################
# Define global options ----------------------------------
###########################################################

### ### ### ###
options(timeout=3000)
options(width = 150)

###########################################################
# Get data -----------------------------------------
###########################################################

### ### ### ###
{
  con_out <- file(tmp <- tempfile(), open = "wb")
  stream_in(
    url("https://md.umwelt.info/dataset/all"), 
    handler = function(df){
      df <- 
      as_tibble(df) |>
      select(any_of(
        c("source", "id", "title", "description", "origins", "bounding_boxes", "tags",
          "source_url", "time_ranges", "organisations", "region")
          )
          ) |>
      unnest(bounding_boxes) |>
      unnest(min, names_sep = "_") |>
      unnest(max, names_sep = "_") |>
      unnest(tags, keep_empty = TRUE) |>
      unnest(Umthes, names_sep = "_", keep_empty = TRUE) |>
      filter(str_detect(Umthes_label, 'Grundwasser')) |>
      filter(str_detect(title, 'Grundwasser|GW-Messstelle|GWÜ|GW-Stand')) |>
      select(-c(Umthes_label, Umthes_id)) |>
      mutate(Coordinate_Type = case_when(min_x == max_x & min_y == max_y ~ "Point")) |>
      filter(Coordinate_Type == "Point") |>
      mutate(longitude = max_x, latitude = max_y) |>
      select(-c(min_x, max_x, min_y, max_y, Coordinate_Type)) |>
      mutate(lat = ifelse(longitude > latitude, longitude, latitude)) |>
      mutate(lon = ifelse(longitude < latitude, longitude, latitude)) |>
      select(-c(longitude, latitude))
      ### ### ### ###
      stream_out(df, con_out, pagesize = 10000)
      }, 
    pagesize = 10000)
  ### ### ### ###
  close(con_out)
  ### ### ### ###
  raw_data <- jsonlite::stream_in(file(tmp))
  unlink(tmp)
}

###########################################################
# Process data -----------------------------------------
###########################################################

### ### ### ###
data <-
  raw_data |> 
  as_tibble() |>
  unnest_wider(any_of(c("organisations", "time_ranges"))) |>
  unnest(cols = any_of(c("name", "role", "origins", "region", "from", "until")), keep_empty = TRUE) |>
  select(-c(RegionalKey, Other, Atkis)) |> 
  unnest(cols = any_of(c("GeoName")), names_sep = "_", keep_empty = TRUE) |>
  separate(origins, into = c("EmptyOrig", "Federal", "State", "Organisation"), sep = "\\/", extra="drop", remove = FALSE) |>
  select(-c(origins, EmptyOrig, websites)) |>
  filter(role %in% c("Operator", "Owner")) |>
  unique() |>
  distinct(id, .keep_all = TRUE) |>
  filter(!(lon < -180 | lon > 180)) |>
  filter(!(lat < -90 | lat > 90))

###########################################################
# Export as GeoJson file ----------------------------------
###########################################################

### ### ### ###
geo_data <-
  data |>
  select(-c(description, role, GeoName_id)) |>
  st_as_sf(coords = c("lat","lon")) |>
  st_set_crs(4326)

### ### ### ###
st_write(geo_data, "Groundwater_Map.geojson")
