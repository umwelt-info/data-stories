###################################################################################
### Install Packages
###################################################################################

### ### ### ###
packages_list <- c("jsonlite", "tidyverse", "scales", "rio", "plotly", "listviewer", "RColorBrewer", "showtext", "extrafont")
new_packages <- packages_list[!(packages_list %in% installed.packages()[,"Package"])]
if(length(new_packages)) install.packages(new_packages)

###################################################################################
### Load Packages
###################################################################################

library(jsonlite);packageVersion("jsonlite")
library(tidyverse);packageVersion("tidyverse")
library(scales)
library(rio)
library(plotly)
library(listviewer)
library(RColorBrewer)
library(showtext)
library(extrafont);packageVersion("extrafont")

###########################################################
# Define global options ----------------------------------
###########################################################

### ### ### ###
options(timeout=3000)
options(width = 150)
# # par(oma=c(4,8,4,8))

###########################################################
# Import corporate design colours for plot ---------------
###########################################################

### ### ### ###
col_cd <- c("#918ae5", "#090066", "#62aaf2", "#0e59a5", "#cc9951", "#663c00", "#aacc00", "#556600", "#252527")

### ### ### ###
text_col <- "#090066"

### ### ### ###
font_add_google(name = "IBM Plex Sans",   # Name of the font on the Google Fonts site
                family = "IBM Plex Sans")
showtext_auto()

###########################################################
# Get data -----------------------------------------
###########################################################

### ### ### ### get datasets for groundwater wells
dataframe <- stream_in(url("https://md.umwelt.info/search/all?query=type:/Messwerte/Grundwasser/Messstellen"), pagesize = 10000)

###########################################################
# Descriptive statistics plots ----------------------------
###########################################################

### ### ### ### extract relevant data 
data <-
  dataframe |> 
  as_tibble() |>
  unnest_wider(any_of(c("organisations", "time_ranges"))) |>
  unnest(cols = any_of(c("name", "role", "origins", "license")), keep_empty = TRUE) |>
  unnest_wider(col = c("from", "until"), names_sep = "_") |>
  distinct(id, .keep_all = TRUE)

### ### ### ### check if entries are unique. check if there are multiple entries per location
length(unique(data$id))

### ### ### ###
data <- data |>
  add_count(id, name = "count_id")

### ### ### ### Licenses per federal state
Bundesländer <- c(
  "Baden-Württemberg" = "BW", "Bayern" = "BY", "Berlin" = "BE",
  "Brandenburg" = "BB", "Bremen" = "HB", "Hamburg" = "HH",
  "Hessen" = "HE", "Mecklenburg-Vorpommern" = "MV", "Niedersachsen" = "NI",
  "Nordrhein-Westfalen" = "NW", "Rheinland-Pfalz" = "RP", "Saarland" = "SL",
  "Sachsen" = "SN", "Sachsen-Anhalt" = "ST", "Schleswig-Holstein" = "SH",
  "Thüringen" = "TH"
)

### ### ### ###
data <- data %>%
  mutate(federal_state = map_chr(str_split(origins, pattern = "/"), function(x) {
    if (length(x) >= 3 && x[2] == "Land") {
      x[3]
    } else {
      NA_character_
    }
  }),
        Bundesländer = Bundesländer[federal_state])

### ### ### ###
licenses <- data %>%
  count(label, federal_state, Bundesländer) %>%
  arrange(n)

### ### ### ###
max_n <- licenses %>%
  group_by(label) %>%
  summarize(max_n = max(n))

### ### ### ###
licenses <- licenses %>%
  left_join(max_n, by = "label")


### ### ### ### Heatmap 
licenses_sum <- licenses %>%
  group_by(federal_state) %>%
  mutate(sum_row = sum(n)) %>%
  ungroup() %>%
  group_by(label) %>%
  mutate(sum_col = sum(n)) %>%
  ungroup()

### ### ### ###
licenses_sorted <- licenses_sum %>%
  mutate(federal_state = factor(federal_state, levels = unique(federal_state[order(sum_row)])),
         label = factor(label, levels = unique(label[order(-sum_col)])))

### ### ### ###

### ### ### ### 
CD_color <- c("#ededfc", "#b5b5f3" , "#918ae5", "#090066")
cols <- colorRampPalette(CD_color)

### ### ### ###
axis_style <- list(
  family = "IBM Plex Sans",
  size = 18,
  color = "#090066"
)

### ### ### ###
tick_style <- list(
  family = "IBM Plex Sans",
  size = 12,
  color = "#090066"
)

### ### ### ###
x_label <- "Anzahl Lizenzen"
y_label <- "Flussgebietseinheit"

### ### ### ###
x.axisSettings <- list(
  title = list(text = x_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
y.axisSettings <- list(
  title = list(text = y_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE, 
  size = 24,
  tickfont = tick_style,
  tickcolor = "#090066"
)


heatmap <- plot_ly() %>% 
  add_trace(
    data = licenses_sorted, 
    x = ~ label, 
    y = ~ federal_state,
    z = ~ licenses_sorted$sum_row,
    colors = colorRamp(c("#ededfc", "#b5b5f3" , "#918ae5", "#090066")),
    type = "heatmap",
    colorbar = list(
    title = "Anzahl Lizenzen",
    thickness = 20,
    tickvals = c(0, 20000, 40000, 60000),
    ticktext = c("0", "20000", "40000", "60000")
),
    hoverinfo = "text",
    hovertext = ~ paste(
      "Lizenstyp :", licenses_sorted$label,
      "<br>Bundesland :", licenses_sorted$federal_state,
      "<br> Anzahl Lizenzen :", licenses_sorted$n)) %>% 
  layout(xaxis = x.axisSettings,
         yaxis = y.axisSettings
                  )

### ### ### ###
dir.create("./Groundwater_Analysis/HTML/", showWarnings = FALSE)
heatmap
htmlwidgets::saveWidget(as_widget(heatmap), "/home/ubuntu/data-stories/Groundwater_Analysis/HTML/Heatmap.html")
plotly_json(heatmap)

### ### ### ### Number of groundwater stations over time
data$from_1 <- as.Date(data$from_1)
data$from_2 <- as.Date(data$from_2)
data$until_1 <- as.Date(data$until_1)
data$until_2 <- as.Date(data$until_2)


### ### ### ###
station_sum <- data |>
  mutate(dates = as.Date(from_1),
        date = day(dates), month = month(dates), year = year(dates),
        active = 1) |>
        group_by(year) |>
        summarise(active_year = sum(active)) |>
        mutate(roll_sum = cumsum(x = active_year))

station_sum_red <- station_sum |> filter(year %in% c(1950, 1980, 2024))

### ### ### ###
station_dev <- 
  data |>
  as_tibble() |> 
  select(from_1) |>
  drop_na(from_1) |>
  mutate(year = year(from_1)) |>
  mutate(active = 1) |> 
  group_by(year) |>
  summarise(active_per_year = sum(active)) |> 
  ### ### ### ###
  full_join(data |>
    select(until_1) |>
    drop_na(until_1) |>
    mutate(year = year(until_1)) |>
    mutate(inactive = 1) |> 
    group_by(year) |> 
    summarise(inactive_per_year = sum(inactive))) |>
  ### ### ### ###
  full_join(data |>
    select(from_2) |>
    drop_na(from_2) |>
    mutate(year = year(from_2)) |>
    mutate(active = 1) |> 
    group_by(year) |> 
    summarise(active_per_year2 = sum(active))) |>
  ### ### ### ###
  full_join(data |>
    select(until_2) |>
    drop_na(until_2) |>
    mutate(year = year(until_2)) |>
    mutate(inactive = 1) |> 
    group_by(year) |> 
    summarise(inactive_per_year2 = sum(inactive))) |>
  ### ### ### ###
  mutate(
    inactive_per_year = replace_na(inactive_per_year, 0),
    inactive_per_year2 = replace_na(inactive_per_year2, 0),
    active_per_year2 = replace_na(active_per_year2, 0)) |>
  mutate(active_stations = cumsum(active_per_year - inactive_per_year + active_per_year2 - inactive_per_year2))  |>
  group_by(year) %>%
  nest() %>%
  mutate(colour = map(data, ~ bind_cols(colour = col_cd[1:nrow(.x)]))) %>%
  unnest(c(data, colour)) %>%
  ungroup() %>%
  filter(!year %in% c(2024, 2025)) #filtered for now, as data is still not up to date


###################################################################################
### Erstellung Plotly manuell
###################################################################################

### ### ### ###
font <- list(
  family = "IBM Plex Sans",
  size = 14,
  color = '#090066')

### ### ### ###

### ### ### ###
max_year <- station_dev$year[which.max(station_dev$active_stations)]
station_dev_red <- station_dev |> filter(year %in% c(1950, max_year, 2023))

station_dev_plotly <- 
  plot_ly() %>% 
  add_trace(
    data = station_dev,
    x = ~ year,
    y = ~ active_stations,
    type = "scatter",
    mode = 'lines',
    textposition = "auto",
    hoverinfo = "text",
    color = ~I(colour),
    hovertext = ~ paste(
      "Jahr :", station_dev$year,
      "<br> Aktive Stationen :", station_dev$active_stations)) %>%
      add_trace(
    data = station_dev_red,
    x = ~ year,
    y = ~ active_stations,
    type = "scatter",
    mode = 'markers',
    textposition = "auto",
    hoverinfo = "text",
    color = ~I(colour),
    hovertext = ~ paste(
      "Jahr :", station_dev_red$year,
      "<br> Aktive Stationen :", station_dev_red$active_stations),
    marker = list(size = ~10, color = "#090066")
      ) %>%
  layout(showlegend = FALSE,
         font = font,
         xaxis =
           list(
             zerolinecolor = '#e5ecf6',
             zerolinewidth = 2,
             gridcolor = '#e5ecf6',
             title = 'Jahr'),
         yaxis =
           list(
             zerolinecolor = '#e5ecf6',
             zerolinewidth = 2,
             gridcolor = '#e5ecf6',
             title = 'Aktive Messstellen mit\nonline verfügbaren Daten',
             tickformat = ".0f") 
  ) #%>% 
  # add_annotations(#ax = c(1940, 1996, 2013), ay = c(5000, 35000, 25000), axref='x', ayref='y', 
  #                 x = station_dev_red$year, y = station_dev_red$active_stations, text = station_dev_red$active_stations#, showarrow = TRUE
  #                 )
station_dev_plotly
plotly_json(station_dev_plotly)

### ### ### ###
sites_per_RBD_SU_plotly
plotly_json(sites_per_RBD_SU_plotly) #copy {"data": [], "layout": {}, "config": {}} into .json-file to make it readable in our CMS

#' The maximum occurs 2002 with 26879 entries. In the last years (especially 2022 and 2023), the number goes down rapidly.
#' The stations likely still exist, but the data is not yet online. This lack of timeliness is a problem we should point out.
#' 2024 could not be included here because the until date cannot be used to infer whether a station was terminated. 


### ### ### ### Oldest stations
data_old <- data |> # older than 100 years and still active
  filter(from_1 <= "1924-06-18" & until_1 >= "2023-12-31") |>
  dplyr::select(-description)

data_old <- data_old|>
  filter(!id %in% c("APP_GWMN_710", "Grundwassermessstelle-Wiederau--Messstellen-Nr---43453350-"))
# the entries from SH are artefacts, the measurements seem to start actually in 1996
# the url from the measurement site in BB is not reachable


###########################################################
# Number of stations per federal state divided by area and population

# csv file was retrieved from https://www.statistikportal.de/de/bevoelkerung/flaeche-und-bevoelkerung on 13.06.24
url <- "https://www.destatis.de/DE/Themen/Laender-Regionen/Regionales/Gemeindeverzeichnis/Administrativ/02-bundeslaender.xlsx?__blob=publicationFile"
ger_pop_area <- rio::import(file = url, sheet = 2) %>% drop_na(1)

ger_pop <- data.frame("federal_state" = ger_pop_area[4:19,1], "Population" = ger_pop_area[4:19,4], "Area" = ger_pop_area[4:19,3]) 

ger_pop <- ger_pop %>%
  mutate(
    federal_state = gsub("^.{0,4}", "", federal_state),
    Population = as.numeric(str_replace(Population, " ", "")),
    Area = as.numeric(str_replace(Area, ",", ".")),
    Population_density = Population/Area
  )

ger_pop <- ger_pop |>
  filter(federal_state !="Deutschland") # exclude Germany as long as we do not have full coverage of all states

entries_per_state <- data |>
  count(federal_state) |>
  rename(number_datasets = n) 
  
ger_gw_pop_area <- full_join(ger_pop, entries_per_state, by = "federal_state") |>
  mutate(datasets_per_pop1000 = number_datasets/(Population/1000)) |>
  mutate(datasets_per_area = number_datasets/Area)|>
  mutate(datasets_per_pop_dens = number_datasets/Population_density) |>
  filter(!is.na(number_datasets))

