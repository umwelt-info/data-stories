###############################################################
# Install Packages -----------------------------------------------
###############################################################

### ### ### ###
install.packages("plotly")
install.packages("showtext")
install.packages("hrbrthemes")
install.packages("cowplot")

###############################################################
# Load Packages -----------------------------------------------
###############################################################

library(ggplot2);packageVersion("ggplot2")
library(tidyverse);packageVersion("tidyverse")
library(plotly);packageVersion("plotly")
library(forcats);packageVersion("forcats")
library(hrbrthemes);packageVersion("hrbrthemes")
library(extrafont);packageVersion("extrafont")
library(cowplot);packageVersion("cowplot")
library(showtext);packageVersion("showtext")

###############################################################
# Create functions --------------------------------------------
###############################################################

### ### ### ###
create_color_palette <- function() {
  ### ### ### ### Background colors
    background <- tibble(
    color_name = c("neutral 100"),
    color_value = c("#fafafa"),
    color_palette = "neutral"
    )
  ### ### ### ### Primary colors
  primary <- tibble(
    color_name = c("primary-500", "primary-900"),
    color_value = c("#918ae5", "#090066"),
    color_palette = "primary"
    )
  ### ### ### ### Secondary colors
  secondary <- tibble(
    color_name = c("secondary-500", "secondary-800"),
    color_value = c("#62aaf2", "#0e59a5"),
    color_palette = "secondary"
    )
  ### ### ### ### Additional colors
  additional <- tibble(
    color_name = c("brown-500", "brown-900", "green-500", "green-900", "black"),
    color_value = c("#cc9951", "#663c00", "#aacc00", "#556600", "#252527"),
    color_palette = "additional"
    )
  ### ### ### ### combine colors
  cp_colors <- tibble(
    color_name = c(primary$color_name, secondary$color_name, additional$color_name, background$color_name),
    color_value = c(primary$color_value, secondary$color_value, additional$color_value, background$color_value),
    color_palette = c(primary$color_palette, secondary$color_palette, additional$color_palette, background$color_palette)
    )
  cp_colors
}

###############################################################
# Add figure themes -------------------------------------------
###############################################################

add_theme <- function(plot) {
  background <- tibble(
    color_name = c("neutral 100"),
    color_value = c("#fafafa"),
    color_palette = "neutral"
    )
  ### ### ### ###
  text <- tibble(
    color_name = "primary-900",
    color_value = "#090066",
    color_palette = "primary"
    )
  ### ### ### ###
  plot + 
    scale_fill_manual("Abstimmung: ", values = df$color_value) +
    scale_x_continuous(expand = c(0, 0)) +
    theme_classic() +
    theme(
      text = element_text(family = "IBM_plex", color = text$color_value),
      panel.background = element_rect(fill = background$color_value),
      plot.background = element_rect(fill = background$color_value),
      axis.text = element_text(size=52, color = text$color_value),

      axis.ticks = element_line(color = text$color_value),
      axis.title = element_text(size=58, color = text$color_value),
      
      axis.line.x = element_line(color = text$color_value),
      axis.text.x = element_text(color = text$color_value),
      
      axis.line.y = element_blank(),
      axis.ticks.y = element_blank(),
      axis.title.y = element_blank(),

      legend.text = element_text(size=42),
      legend.title = element_text(size=48),
      legend.position = "right",
      legend.background = element_rect(fill = "transparent", colour = "transparent"),
      legend.box.background = element_rect(fill = "transparent", colour = "transparent"),
      )
}

###############################################################
# Add font ----------------------------------------------------
###############################################################

### ### ### ###
font_add_google(name = "IBM Plex Sans",   # Name of the font on the Google Fonts site
                family = "IBM_plex")

###############################################################
# Create dataframe --------------------------------------------
###############################################################

### ### ### ###
cp_colors <- create_color_palette()
p1 <- cp_colors %>% filter(color_name == "primary-900")
p2 <- cp_colors %>% filter(color_name == "primary-500")

### ### ### ###
mycols = bind_rows(p1, p2)

### ### ### ###
df <- 
  tibble(
    Schema = c("CSW", "Dublin core", "RDF / DCAT-AP",  "CKAN", "schema.org"),
    Ja = c(6, 7, 8, 10, 11),
    Vielleicht = c(6, 6, 4, 3, 4)
  ) %>%
  arrange(Ja) %>%
  pivot_longer(!Schema, names_to = "Abstimmung", values_to = "Werte") %>%
  cbind(mycols) 

### ### ### ###
df$Schema <- factor(df$Schema,
  levels = unique(df$Schema))

###############################################################
# Create plots article "Über den Metadatenexport" -------------
###############################################################

### ### ### ###
metadatenexport <- ggplot(df, aes(x = Werte, y = Schema, group = Schema, fill = Abstimmung)) +
  geom_bar(stat = "identity", width = .7) +
  labs(x = "Anzahl Stimmen", y = "") 

### ### ### ###
showtext_auto()
final_plot <- add_theme(metadatenexport)
ggsave("Plots/Abstimmung_Plot.png", dpi = 700, width = 9, height = 4)
# file1 <- tempfile("file1", fileext = ".png", tmpdir = getwd())
# save_plot(file1, maia, base_asp = 2.3)
showtext_auto(FALSE)

#######################################################################
# Create plots article "Über das Ranking unserer Suchergebnisse" ------
#######################################################################

### ### ### ###
df1 <- tibble(
  x = c("A", "B", "C", "D"),
  y = c(3, 9, 5, 7),
  order = c(1, 2, 3, 4),
  color_value = c("#090066", "#0e59a5", "#556600", "#663c00"),
  plot = "Plot1"
  )
### ### ### ###
df1 <- df1 %>% arrange(order)
df1$x <- factor(df1$x, levels = unique(df1$x))

### ### ### ###  
df2 <- tibble(
  x = c("A", "B", "C", "D", "D"),
  y = c(3, 9, 5, 4, 3),
  # order = c(1, 2, 3, 4, 5),
  order = c(5, 6, 7, 8, 9),
  color_value = c("#090066", "#0e59a5", "#556600", "#663c00", "#cc9951"),
  plot = "Plot2"
  ) 
### ### ### ###
df2 <- df2 %>% arrange(order)
df2$x <- factor(df2$x, levels=unique(df2$x))

### ### ### ###  
df3 <- tibble(
  x = c("A", "B", "C", "D"),
  y = c(3, 9, 5, 4),
  color_value = c("#090066", "#0e59a5", "#556600", "#663c00"),
  # order = c(4, 1, 2, 3),
  order = c(13, 10, 11, 12),
  plot = "Plot3"
  )
### ### ### ###
df3 <- df3 %>% arrange(order)
df3$x <- factor(df3$x, levels = unique(df3$x))



bg_col <- tibble(
    color_name = c("neutral 100"),
    color_value = c("#fafafa"),
    color_palette = "neutral"
    )
### ### ### ###
  text_col <- tibble(
    color_name = "primary-900",
    color_value = "#090066",
    color_palette = "primary"
    )
### ### ### ###
showtext_auto()
fig1 <- 
  ggplot(data = df1, aes(x = x, y = y, fill = color_value, order = order)) +
    geom_col(width = .7, position = position_stack(reverse = TRUE)) +
    labs(x = "", y = "Metadatenqualitäts-Score") +
    scale_fill_identity() +
  theme_classic() +
  theme(
    text = element_text(family = "IBM_plex", color = text_col$color_value),
    panel.background = element_rect(fill = bg_col$color_value),
    plot.background = element_rect(fill = bg_col$color_value),
    axis.title = element_text(size=58, color = text$color_value),
    axis.text = element_text(size=52, color = text$color_value),
    axis.ticks = element_line(color = text$color_value),

    axis.text.x = element_text(color = text$color_value),
    axis.line.x = element_blank(),

    axis.line.y = element_line(color = text$color_value),
    axis.ticks.y = element_blank(),
    axis.title.y = element_blank(),

    legend.text = element_text(size=42),
    legend.title = element_text(size=48),
    axis.text.y = element_blank(),
    # axis.line.y = element_blank(),
    axis.title.x = element_blank(),
    axis.ticks.x = element_blank(),
    legend.box.background = element_rect(fill = bg_col$color_value, colour = bg_col$color_value),
    legend.background = element_rect(fill = bg_col$color_value, colour = bg_col$color_value)
    )  +
  theme(legend.position = "top")

### ### ### ###
fig2 <- 
  ggplot(data = df2, aes(x = x, y = y, fill = color_value, order = order)) +
    geom_col(width = .7, position = position_stack(reverse = TRUE)) +
    scale_fill_identity() +
    theme_classic() +
    theme(
        text = element_text(family = "IBM_plex", color = text_col$color_value),
    panel.background = element_rect(fill = bg_col$color_value),
    plot.background = element_rect(fill = bg_col$color_value),
    axis.title = element_text(size=58, color = text$color_value),
    axis.text = element_text(size=52, color = text$color_value),
    axis.ticks = element_line(color = text$color_value),

    axis.text.x = element_text(color = text$color_value),
    axis.line.x = element_blank(),

    axis.line.y = element_line(color = text$color_value),
    axis.ticks.y = element_blank(),
    axis.title.y = element_blank(),

    legend.text = element_text(size=42),
    legend.title = element_text(size=48),
    axis.text.y = element_blank(),
    # axis.line.y = element_blank(),
    axis.title.x = element_blank(),
    axis.ticks.x = element_blank(),
    legend.box.background = element_rect(fill = bg_col$color_value, colour = bg_col$color_value),
    legend.background = element_rect(fill = bg_col$color_value, colour = bg_col$color_value)
      ) +
    theme(legend.position = "top")

### ### ### ###
fig3 <- 
  ggplot(data = df3, aes(x = x, y = y, fill = color_value, order = order)) +
    geom_col(width = .7, position = position_stack(reverse = TRUE)) +
    scale_fill_identity() +
    theme_classic() +
    theme(
        text = element_text(family = "IBM_plex", color = text_col$color_value),
    panel.background = element_rect(fill = bg_col$color_value),
    plot.background = element_rect(fill = bg_col$color_value),
    axis.title = element_text(size=58, color = text$color_value),
    axis.text = element_text(size=52, color = text$color_value),
    axis.ticks = element_line(color = text$color_value),

    axis.text.x = element_text(color = text$color_value),
    axis.line.x = element_blank(),

    axis.line.y = element_line(color = text$color_value),
    axis.ticks.y = element_blank(),
    axis.title.y = element_blank(),

    legend.text = element_text(size=42),
    legend.title = element_text(size=48),
    axis.text.y = element_blank(),
    # axis.line.y = element_blank(),
    axis.title.x = element_blank(),
    axis.ticks.x = element_blank(),
    legend.box.background = element_rect(fill = bg_col$color_value, colour = bg_col$color_value),
    legend.background = element_rect(fill = bg_col$color_value, colour = bg_col$color_value)
      ) +
    theme(legend.position = "top")

#######################################################################
# Combine plots article "Über das Ranking unserer Suchergebnisse" -----
#######################################################################

### ### ### ###
showtext_auto()
plots <- plot_grid(
  fig1, 
  fig2, 
  fig3, 
  nrow = 1, 
  labels = c("1", "2", "3"), 
  label_size = 54,
  label_fontface = "plain",
  label_colour = text_col$color_value,
  label_fontfamily = "IBM_plex")

ggsave("Plots/Ranking_Plot.png", dpi = 700, width = 9, height = 4)

# file1 <- tempfile("file1", fileext = ".png", tmpdir = getwd())
# save_plot(file1, plots, base_asp = 2.3)
showtext_auto(FALSE)


### ### ### ###
final_plot <- add_sub(plots, 
  "1) die Suchergebnisse erhalten einen Score basierend auf BM25", 
  size = 28,
  x = 0, 
  hjust = 0,
  color = text_col$color_value,
  fontfamily = "IBM_plex")

### ### ### ###
final_plot <- add_sub(final_plot,
  "2) Suchergebnis 'D' wird abgewertet, da es den Status 'obsolet' besitzt",
  size = 28,
  x = 0, 
  hjust = 0,
  color = text_col$color_value,
  fontfamily = "IBM_plex")

### ### ### ###
final_plot <- add_sub(final_plot,
  "3) die Suchergebnisse werden anhand ihres Scores absteigend sortiert", 
  size = 28,
  x = 0, 
  hjust = 0,
  color = text_col$color_value,
  fontfamily = "IBM_plex")

### ### ### ###
final_plot <- ggdraw(final_plot)

### ### ### ###
file1 <- tempfile("file1", fileext = ".png", tmpdir = getwd())
save_plot(file1, final_plot, base_asp = 2.3)
showtext_auto(FALSE)




