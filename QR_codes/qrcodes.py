import segno

# QR code for umwelt.info website link

data = "https://umwelt.info"


qrcode = segno.make_qr(data, error="M")
qrcode.save(
    "ui_qrcode.svg",
    scale=22,
    unit="mm",
    border=0,
    dark=(9, 0, 102),
)

# QR code for business cards in u.i CD
data = """BEGIN:VCARD
VERSION:4.0
N:Mustermensch;Manu
FN:Manu Mustermensch
ORG:Nationales Zentrum für Umwelt- und Naturschutzinformationen
URL:https://umwelt.info
EMAIL:manu.mustermensch@uba.de
TEL:+49 340 210 0
ADR:;;Gotthardstraße 34/37;Merseburg;;06217;Deutschland
END:VCARD"""

qrcode = segno.make_qr(
    data, error="M"
)  # number of recoverable information; M(edium) or L(ow) work fine one small print products
qrcode.save(
    "ManuMustermensch_vCard.svg",
    scale=22,
    unit="mm",
    border=0,
    dark=(9, 0, 102),
)
