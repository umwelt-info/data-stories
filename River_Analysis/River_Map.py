from datetime import date

import geojson

from fetch import fetch


def format_time_range(time_range):
    from_ = date.fromisoformat(time_range["from"])
    until = date.fromisoformat(time_range["until"])

    return f"von {from_:%d.%m.%Y} bis {until:%d.%m.%Y}"


def format_region(region):
    if "Other" in region:
        return region["Other"]
    elif "GeoName" in region:
        return region["GeoName"]["label"]
    elif "RegionalKey" in region:
        return region["RegionalKey"]["label"]
    elif "Watershed" in region:
        return region["Watershed"]["label"]
    else:
        raise Exception(f"Unexpected kind of region: {region}")


def build_feature(dataset):
    bounding_box = dataset["bounding_boxes"][0]
    min_ = bounding_box["min"]
    max_ = bounding_box["max"]

    x = (min_["x"] + max_["x"]) / 2
    y = (min_["y"] + max_["y"]) / 2

    geometry = geojson.Point((x, y))

    properties = {
        "federal_state": dataset["origins"][0].split("/")[2],
    }

    description = dataset.get("description", "")
    if len(description) > 250:
        description = description[:247] + "..."

    time_ranges = (
        ", ".join(
            format_time_range(time_range) for time_range in dataset["time_ranges"]
        )
        if "time_ranges" in dataset
        else "N/A"
    )

    regions = (
        ", ".join(format_region(region) for region in dataset["regions"])
        if "regions" in dataset
        else "N/A"
    )

    popup = f"""<h3><a href="{dataset["source_url"]}">{dataset["title"]}</a></h3>
<p>{description}</p>
<p>Bundesland: {properties["federal_state"]}</p>
<p>Zeitraum: {time_ranges}</p>
<p>Ortsbezug: {regions}</p>
<p><a href="/suche?detail={dataset["source"]}%2F{dataset["id"]}">zum Suchergebnis</a></p>"""

    properties["popup"] = popup

    return geojson.Feature(geometry=geometry, properties=properties)

    # https://md.umwelt.info/search?types_root=/Messwerte/Flüsse&topics_root=/Wasser


if __name__ == "__main__":
    datasets = fetch(query="type:/Messwerte/Flüsse/Messstellen")
    # datasets = fetch(query="types_root=/Messwerte/Flüsse")

    features = list(
        map(
            build_feature,
            filter(
                lambda dataset: "bounding_boxes" in dataset,
                datasets,
            ),
        )
    )

    print(f"built {len(features)} features")

    with open("River_Map.geojson", "w") as file:
        geojson.dump(geojson.FeatureCollection(features), file)
        print(f"Saved to {file.name}")
