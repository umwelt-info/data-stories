###################################################################################
### Install Packages under Terminal
###################################################################################

### ### ### ### copy and paste the following commands in terminal
sudo apt-get install gfortran
sudo apt-get install libblas-dev liblapack-dev
sudo apt -y install libudunits2-dev
sudo apt install libpq-dev 
sudo apt install aptitude
sudo aptitude install libgdal-dev 

###################################################################################
### Install Packages
###################################################################################

### ### ### ###
packages_list <- c("gt", "jsonlite", "tidyverse", "scales", "rio", "httr2", "KernSmooth", "sf", "showtext", "hrbrthemes", "patchwork","plotly","listviewer", "extrafont", "htmlwidgets")
new_packages <- packages_list[!(packages_list %in% installed.packages()[,"Package"])]
if(length(new_packages)) install.packages(new_packages)


###################################################################################
### Load Packages
###################################################################################

### ### ### ###
library(gt)
library(jsonlite);packageVersion("jsonlite")
library(tidyverse);packageVersion("tidyverse")
library(scales);packageVersion("scales")
library(rio);packageVersion("rio")
library(httr2);packageVersion("httr2")
library(httr2);packageVersion("KernSmooth")
library(sf);packageVersion("sf")
library(showtext);packageVersion("showtext")
library(hrbrthemes);packageVersion("hrbrthemes")
library(patchwork);packageVersion("patchwork")
library(plotly);packageVersion("plotly")
library(listviewer);packageVersion("listviewer")
library(extrafont);packageVersion("extrafont")
library(htmlwidgets);packageVersion("htmlwidgets")

###########################################################
# Define global options ----------------------------------
###########################################################

### ### ### ###
options(timeout=3000)
options(width = 150)

###########################################################
# Download river basin districts --------------------------
###########################################################

# see https://hub.arcgis.com/maps/2a2ec109580a414486f61962cf2e0e5e/about for details on the downloaded data. The data is provided by the Bundesanstalt für Gewässerkunde (BfG).

### ### ### ###
river_basin_disctricts <-
  "https://services2.arcgis.com/jUpNdisbWqRpMo35/arcgis/rest/services/Flussgebietsgrenzen_und_Flusseinzugsgebiete/FeatureServer/replicafilescache/Flussgebietsgrenzen_und_Flusseinzugsgebiete_-5628970459147804961.csv" %>%
  request() %>%
  req_perform() %>%
  resp_body_string() %>%
  read_csv() %>%
  as_tibble() %>%
  select(
    object_id = OBJECTID,
    RBD_ID = "River basin district ID",
    RBD_NAME = Name,
    RBD_shp_leng = SHAPE_Leng,
    RBD_shp_area = Shape__Area,
    RBD_shp_len = Shape__Length)

###########################################################
# Download river basin subunit districts shapefile---------
###########################################################

### ### ### ### create tempfiles
temp <- tempfile()
temp2 <- tempfile()

### ### ### ### Download the zip file and save to tempfile
URL <- "https://services2.arcgis.com/jUpNdisbWqRpMo35/arcgis/rest/services/Flussgebietsgrenzen_und_Flusseinzugsgebiete/FeatureServer/replicafilescache/Flussgebietsgrenzen_und_Flusseinzugsgebiete_3993221857082270589.zip"
download.file(URL, temp)

### ### ### ### Unzip contents in tempfile
unzip(zipfile = temp, exdir = temp2)

### ### ### ### Read the shapefile
river_basin_subunit_districts_sf <-
  sf::read_sf(temp2) %>%
  select(
    object_id = OBJECTID_1,
    RBD_ID,
    RBD_SU_ID,
    RBD_SU_NAME = nameText,
    LAND = LAND_CD,
    RBD_SU_ID_SHAPE_Leng = SHAPE_Leng)

###########################################################
# Download river basin shapefile --------------------------
###########################################################

### ### ### ### create tempfiles
temp <- tempfile()
temp2 <- tempfile()

### ### ### ### Download the zip file and save to tempfile
URL <- "https://services2.arcgis.com/jUpNdisbWqRpMo35/arcgis/rest/services/Flussgebietsgrenzen_und_Flusseinzugsgebiete/FeatureServer/replicafilescache/Flussgebietsgrenzen_und_Flusseinzugsgebiete_-1542793191985115498.zip"
download.file(URL, temp)

### ### ### ### Unzip contents in tempfile
unzip(zipfile = temp, exdir = temp2)

### ### ### ### Read the shapefile
river_basins_sf <- sf::read_sf(temp2)

###########################################################
# Download rivers in Germany shapefile --------------------
###########################################################

### ### ### ### create tempfiles
temp <- tempfile()
temp2 <- tempfile()

### ### ### ### Download the zip file and save to tempfile
URL <- "https://services2.arcgis.com/jUpNdisbWqRpMo35/arcgis/rest/services/Gew%C3%A4ssernetz_Deutschland/FeatureServer/replicafilescache/Gew%C3%A4ssernetz_Deutschland_3927369764709489372.zip"
download.file(URL, temp)

### ### ### ### Unzip contents in tempfile
unzip(zipfile = temp, exdir = temp2)

### ### ### ### Read the shapefile
rivers_sf <- sf::read_sf(temp2)

#############################################################################
# Fetch umwelt.info API and join river basin subunit districts --------------
#############################################################################

### ### ### ### get datasets for river sampling sites
dataframe <-
  stream_in(url("https://md.umwelt.info/search/all?query=type:/Messwerte/Fl%C3%BCsse/Messstellen"), pagesize = 10000) %>%
  drop_na(bounding_boxes) %>%
  unnest_wider(any_of(c("regions")), names_sep = "_") %>%
  unnest(cols = any_of(c("name", "role", "origins", "license")), keep_empty = TRUE) %>%
  select(source:description, regions_Watershed, label, bounding_boxes) %>%
  add_count(id, name = "count_id") %>%
  unnest(regions_Watershed, names_sep = "_", keep_empty = TRUE) %>%
  distinct(id, .keep_all = TRUE) %>%
  rename(RBD_SU_ID = regions_Watershed_id) %>%
  left_join(river_basin_subunit_districts_sf %>% select(RBD_ID, RBD_SU_ID, RBD_SU_NAME) %>% st_drop_geometry()) %>%
  as_tibble() %>%
  select(source, id, title, RBD_ID, RBD_SU_ID, RBD_SU_NAME, label, count_id, bounding_boxes)

###########################################################
# create shapefile from umwelt.info dataset ---------------
###########################################################

### ### ### ###
data_sf <-
  dataframe %>%
  unnest_wider(bounding_boxes) %>%
  unnest_wider(min) %>%
  unnest_wider(x, names_sep = "min_") %>%
  unnest_wider(y, names_sep = "min_") %>%
  unnest_wider(max) %>%
  unnest_wider(x, names_sep = "max_") %>%
  unnest_wider(y, names_sep = "max_") %>%
  select(source:count_id, x = xmin_1, y = ymin_1) %>%
  st_as_sf(
    x = .,
    coords = c("x", "y"),
    crs = 4326)

### ### ### ### transfrom to crs
data_sf <-
  st_transform(
    data_sf,
    crs = st_crs(river_basins_sf)
  )

###########################################################
# Match sampling sites and river basin polygons -----------
###########################################################

### ### ### ### find points within RB polygons
data_match_rb_sf <-
  st_intersection(river_basins_sf, data_sf)

### ### ### ### visualise results in map
ggplot() +
  geom_sf(data = river_basins_sf, col = "red") +
  geom_sf(data = data_match_rb_sf)

### ### ### ### find points within RBU polygons
data_match_rbu_sf <-
  st_intersection(river_basin_subunit_districts_sf, data_match_rb_sf)

### ### ### ### visualise results in map
ggplot() +
  geom_sf(data = river_basin_subunit_districts_sf, col = "red") +
  geom_sf(data = data_match_rbu_sf)

### ### ### ### find points within RBD_SU polygons
data_match_rbu <-
  data_match_rbu_sf %>%
  st_drop_geometry() %>%
  left_join(river_basin_disctricts, by = "RBD_ID") %>%
  relocate(RBD_NAME, .after = RBD_ID)

###########################################################
# Create final dataframe ----------------------------------
###########################################################

### ### ### ###
df_final <-
  data_match_rbu %>%
  select(source, title, id, label, count_id, RBD_ID, RBD_NAME, RBD_SU_ID, RBD_SU_NAME, RBD_SU_ID_SHAPE_Leng, LAND,
         NAME_2500, GEBKZ_2500, AREA_2500K, AEO_2500KM,
         NAME_1000, GEBKZ_1000, AREA_1000K, AEO_1000KM,
         NAME_500, GEBKZ_500, AREA_500KM) %>%
         mutate(Datenquelle = case_when(
          source == "geoportal-wasser-sl" ~ "GeoPortal Saarland", 
          source == "hvz-bw" ~ "Hochwasservorhersagezentrale Baden-Württemberg",  
          source == "gkd-by" ~ "Gewässerkundlicher Dienst Bayern",
          source == "fis-wasser-mv" ~ "Fachinformationssystem Gewässer Mecklenburg-Vorpommern",
          source == "hochwasservorhersagezentrale-st" ~ "Hochwasservorhersagezentrale Sachsen-Anhalt",
          source == "nid-by" ~ "Niedrigwasser-Informationsdienst Bayern",
          source == "undine-bfg" ~ "Informationsplattform Undine",
          source == "pegelonline-gdws" ~ "Pegelonline",
          source == "niz-bw" ~ "Niedrigwasser-Informationszentrum Baden-Württemberg",
          source == "messpegel-de" ~ "Messpegel.de",
          source == "hnd-by" ~ "Hochwassernachrichtendienst Bayern",
          source == "hlnug-he" ~ "Hessisches Landesamt für Naturschutz, Umwelt und Geologie"
          ))

###########################################################
# Import corporate design colours for plot ---------------
###########################################################

### ### ### ###
col_cd <- c("#918ae5", "#090066", "#62aaf2", "#0e59a5", "#cc9951", "#663c00", "#aacc00", "#556600", "#252527")

### ### ### ###
text_col <- "#090066"

### ### ### ###
font_add_google(name = "IBM Plex Sans",   # Name of the font on the Google Fonts site
                family = "IBM Plex Sans")

#########################################################
# Plotly heatmap for licenses per River basi districts
###########################################################

### ### ### ###
river_licenses <- 
  df_final %>%
  count(Datenquelle, label, RBD_NAME) %>%
  arrange(n) %>% 
  rename("Messstellen" = n) %>% 
  left_join(df_final %>%
              count(source, label, RBD_NAME) %>%
              arrange(n) %>% 
              rename("Messstellen" = n) %>%
              group_by(label, RBD_NAME) %>%
              summarize(sum_n = sum(Messstellen))
              ) %>% 
  group_by(label) %>% 
  arrange(desc(sum_n)) %>% 
  mutate(label = factor(label, levels = c("cc-by/4.0", "other-closed", "all-rights-reserved", "cc-by-sa/3.0"))) %>% 
  mutate(RBD_NAME = factor(RBD_NAME, levels = c("Eider", "Ems", "Schlei/Trave", "Oder", "Warnow/Peene", "Weser", "Elbe", "Rhein", "Donau")))  

### ### ### ###
axis_style <- list(
  family = "IBM Plex Sans",
  size = 18,
  color = "#090066"
)

### ### ### ###
tick_style <- list(
  family = "IBM Plex Sans",
  size = 12,
  color = "#090066"
)

### ### ### ###
x_label <- "Anzahl Lizenzen"
y_label <- "Flussgebietseinheit"

### ### ### ###
x.axisSettings <- list(
  title = list(text = x_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
y.axisSettings <- list(
  title = list(text = y_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE, 
  size = 24,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
legend.Settings <- list(
  font = fonts
)

### ### ### ###
heatmap <- plot_ly() %>% 
  add_trace(
    data = river_licenses, 
    x = ~ label, 
    y = ~ RBD_NAME,
    z = ~ river_licenses$sum_n,
    colors = colorRamp(c("#ededfc", "#b5b5f3" , "#918ae5", "#090066")),
    type = "heatmap",
    colorbar = list(title = "Anzahl Lizenzen"),
    hoverinfo = "text",
    hovertext = ~ paste(
      "Lizenstyp :", river_licenses$label,
      "<br>Flussgebietseinheit :", river_licenses$RBD_NAME,
      "<br> Anzahl Messstellen :", river_licenses$sum_n)) %>% 
  layout(xaxis = x.axisSettings,
         yaxis = y.axisSettings,
         legend = legend.Settings)

### ### ### ###
dir.create("./River_Analysis/HTML/", showWarnings = FALSE)
heatmap
htmlwidgets::saveWidget(as_widget(heatmap), "/home/ubuntu/data-stories/River_Analysis/HTML/Heatmap.html")
plotly_json(heatmap)

######################################################################################################
### Plotly figure for number of sampling sites per river basin districts and river basin sub-districts
######################################################################################################

### ### ### ###
sites_per_RBD_SU <-
  df_final %>% 
  select(RBD_ID, Flussgebietseinheit = RBD_NAME, RBD_SU_ID, Flussgebietsuntereinheit = RBD_SU_NAME) %>%
  unique() %>%
  left_join(df_final %>% group_by(Flussgebietsuntereinheit = RBD_SU_NAME, RBD_SU_ID) %>% count(name = 'Messstellen')) %>%
  arrange(Flussgebietseinheit, RBD_SU_ID) %>%
  group_by(Flussgebietseinheit) %>%
  mutate(n2 = sum(Messstellen)) %>%
  ungroup() %>%
  arrange(n2) %>%
  mutate(Flussgebietseinheit = factor(Flussgebietseinheit, levels = unique(Flussgebietseinheit))) %>%
  mutate(Flussgebietsuntereinheit = factor(Flussgebietsuntereinheit, levels = unique(Flussgebietsuntereinheit))) %>% 
  group_by(Flussgebietseinheit) %>% 
  nest() %>% 
  mutate(colour = map(data, ~ bind_cols(colour = col_cd[1:nrow(.x)]))) %>% 
  unnest(c(data, colour)) %>% 
  mutate(Flussgebietseinheit = factor(Flussgebietseinheit, levels = c("Eider", "Ems", "Schlei/Trave", "Oder", "Warnow/Peene", "Weser", "Elbe", "Rhein", "Donau"))) 

### ### ### ###
x_label <- "Anzahl Messstellen"
y_label <- "Flussgebietseinheit"

### ### ### ###
axis_style <- list(
  family = "IBM Plex Sans",
  size = 18,
  color = "#090066"
)

### ### ### ###
tick_style <- list(
  family = "IBM Plex Sans",
  size = 14,
  color = "#090066"
)

### ### ### ###
x.axisSettings <- list(
  title = list(text = x_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
y.axisSettings <- list(
  title = list(text = y_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE, 
  size = 24,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
legend.Settings <- list(
  font = fonts
)

### ### ### ###
marker_style <- list(line = list(width = 0.5,
                                 color = 'rgb(0, 0, 0)'));

### ### ### ###
sites_per_RBD_SU_plotly <- 
  plot_ly() %>% 
  add_trace(
    data = sites_per_RBD_SU, 
    x = ~ Messstellen, 
    y = ~ Flussgebietseinheit,
    type = "bar",
    textposition = "auto",
    hoverinfo = "text",
    color = ~ I(colour),
    marker = marker_style,
    hovertext = ~ paste(
      "Flussgebietseinheit: ", sites_per_RBD_SU$Flussgebietseinheit,
      "<br>Flussgebietsuntereinheit: ", sites_per_RBD_SU$Flussgebietsuntereinheit,
      "<br> Anzahl Messstellen: ", sites_per_RBD_SU$Messstellen)) %>% 
  layout(xaxis = x.axisSettings,
         yaxis = y.axisSettings,
         plot_bgcolor = "#e5ecf6")

### ### ### ###
sites_per_RBD_SU_plotly
htmlwidgets::saveWidget(as_widget(sites_per_RBD_SU_plotly), "/home/ubuntu/data-stories/River_Analysis/HTML/Sites_per_RBD_SU.html")
plotly_json(sites_per_RBD_SU_plotly)

######################################################################################################
### Plotly figure for number of sampling sites per river basin districts and river basin sub-districts
######################################################################################################

### ### ### ###
x_label <- "Anzahl Messstellen"
y_label <- "Flussgebietseinheit"

### ### ### ###
axis_style <- list(
  family = "IBM Plex Sans",
  size = 18,
  color = "#090066"
)

### ### ### ###
tick_style <- list(
  family = "IBM Plex Sans",
  size = 14,
  color = "#090066"
)

### ### ### ###
x.axisSettings <- list(
  title = list(text = x_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
y.axisSettings <- list(
  title = list(text = y_label, font = axis_style),
  zeroline = TRUE,
  showline = TRUE,
  linecolor = "#090066",
  showticklabels = TRUE,
  showgrid = FALSE, 
  size = 24,
  tickfont = tick_style,
  tickcolor = "#090066"
)

### ### ### ###
legend.Settings <- list(
  font = fonts
)

### ### ### ###
col_cd2 <- tibble(colour = rep(col_cd, 10)) %>% mutate(id = 1:nrow(.))

### ### ### ###
marker_style <- list(line = list(width = 0.5,
                                 color = 'rgb(0, 0, 0)'));

### ### ### ###
sites_per_RBD_SU_source <-
  df_final %>% 
  select(source, RBD_ID, Flussgebietseinheit = RBD_NAME, RBD_SU_ID, Flussgebietsuntereinheit = RBD_SU_NAME, Datenquelle) %>%
  unique() %>%
  left_join(df_final %>% group_by(Flussgebietsuntereinheit = RBD_SU_NAME, RBD_SU_ID, source) %>% count(name = 'Messstellen')) %>%
  arrange(Flussgebietseinheit, RBD_SU_ID, source) %>%
  group_by(Flussgebietseinheit, source) %>%
  mutate(n2 = sum(Messstellen)) %>%
  ungroup() %>%
  group_by(Flussgebietsuntereinheit) %>%
  nest() %>% 
  ungroup() %>% 
  mutate(id = 1:nrow(.)) %>% 
  left_join(tibble(colour = rep(col_cd, 10)) %>% mutate(id = 1:nrow(.))) %>% 
  unnest(c(data, colour)) %>% 
  ungroup() %>%
  arrange(Flussgebietsuntereinheit) %>% 
  mutate(Flussgebietseinheit = factor(Flussgebietseinheit, levels = c("Eider", "Ems", "Schlei/Trave", "Oder", "Warnow/Peene", "Weser", "Elbe", "Rhein", "Donau"))) %>%  
  mutate(Flussgebietsuntereinheit = factor(Flussgebietsuntereinheit, levels = unique(Flussgebietsuntereinheit)))  
  
### ### ### ###
sites_per_RBD_SU_source_plotly <- 
  plot_ly() %>% 
  add_trace(
    data = sites_per_RBD_SU_source, 
    x = ~ Messstellen, 
    y = ~ Flussgebietseinheit,
    type = "bar",
    textposition = "auto",
    hoverinfo = "text",
    color = ~ I(colour),
    marker = marker_style,
    hovertext = ~ paste(
      "Flussgebietseinheit: ", sites_per_RBD_SU_source$Flussgebietseinheit,
      "<br>Flussgebietsuntereinheit: ", sites_per_RBD_SU_source$Flussgebietsuntereinheit,
      "<br> Anzahl Messstellen: ", sites_per_RBD_SU_source$Messstellen,
      "<br> Datenquelle: ", sites_per_RBD_SU_source$Datenquelle)) %>% 
  layout(xaxis = x.axisSettings,
         yaxis = y.axisSettings,
         plot_bgcolor = "#e5ecf6")

### ### ### ###
sites_per_RBD_SU_source_plotly
htmlwidgets::saveWidget(as_widget(sites_per_RBD_SU_source_plotly), "/home/ubuntu/data-stories/River_Analysis/HTML/Sites_per_RBD_SU_Sources.html")
plotly_json(sites_per_RBD_SU_source_plotly)
