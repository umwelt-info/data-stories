import os
from datetime import datetime
import json
from urllib.parse import urljoin

import requests

from lib.ui_logging import logging

SEARCH_URL_LOCAL = "http://localhost:8081"
SEARCH_URL_REMOTE = "https://md.umwelt.info"
SEARCH_URL_DEFAULT = SEARCH_URL_REMOTE

session = requests.Session()


def fetch(url=SEARCH_URL_DEFAULT, query="*", sampling_fraction=None):
    global session
    logging.debug(f"Inquire {url} for '{query}'")
    params = {
        "query": query,
    }

    if sampling_fraction is not None:
        params["sampling_fraction"] = sampling_fraction

    with session.get(
        urljoin(url, "search/all"), params=params, stream=True
    ) as response:
        response.raise_for_status()

        for line in response.iter_lines():
            dataset = json.loads(line)
            yield dataset


def _fetch_page(page, url, query, **kwargs):
    global session
    logging.debug(f"Inquire {url} for '{query}'")
    params = {"query": query, "page": page, "results_per_page": 100}
    params.update(kwargs)

    response = session.get(
        urljoin(url, "search"),
        params=params,
        headers={"accept": "application/json"},
    )

    response.raise_for_status()

    return response.json()


def fetch_ranked(*, url, query, **kwargs):
    logging.debug(
        "Running query with paged API interface to ensure rank in the dataset is equivalent to the rank the search engine decides."
    )

    results = _fetch_page(1, url, query, **kwargs)

    def iter():
        nonlocal results

        for result in results["results"]:
            yield result

        for page in range(2, results["pages"] + 1):
            results = _fetch_page(page, url, query, **kwargs)

            for result in results["results"]:
                yield result

    return iter()


# Define the directory where JSON files are stored
LOCAL_DATASET_DIR = "/home/ubuntu/metadaten/harvester/regression-test/datasets"


def fetch_local(directory=LOCAL_DATASET_DIR):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith(".json"):
                file_path = os.path.join(root, file)
                with open(file_path, "r") as f:
                    dataset = json.load(f)
                    yield dataset


def fetch_dataframe(
    url=SEARCH_URL_DEFAULT,
    query="*",
    sampling_fraction=None,
    exclude=None,
    columns=None,
    build_row=None,
    filter_datasets=None,
    **kwargs,
):
    from pandas import DataFrame

    if kwargs:
        iter = fetch_ranked(url=url, query=query, **kwargs)
    else:
        iter = fetch(
            url=url, query=query, sampling_fraction=sampling_fraction, **kwargs
        )

    if filter_datasets:
        iter = filter(filter_datasets, iter)

    if build_row:
        iter = map(build_row, iter)

    for req_field in ["source", "id"]:
        if columns and req_field not in columns:
            columns.append(req_field)

    return DataFrame.from_records(
        iter, index=["source", "id"], exclude=exclude, columns=columns
    )


def fetch_polars(
    url=SEARCH_URL_DEFAULT,
    query="*",
    sampling_fraction=None,
    build_row=None,
    **kwargs,
):
    """Read metadata index into a polars.DataFrame

    to pre-select the data used from our metadata schema, provide a function
    build_row that converts a single dataset into a Dict of the values you need.
    """
    import polars

    if kwargs:
        datasets = fetch_ranked(url=url, query=query, **kwargs)
    else:
        datasets = fetch(
            url=url, query=query, sampling_fraction=sampling_fraction, **kwargs
        )

    if build_row:
        datasets = map(build_row, datasets)

    return polars.DataFrame(datasets, strict=False)


def time_ranges_minmax(time_ranges):
    if not time_ranges:
        return None, None
    from_ = min(datetime.strptime(t["from"], "%Y-%m-%d") for t in time_ranges)
    until_ = max(datetime.strptime(t["until"], "%Y-%m-%d") for t in time_ranges)
    return from_, until_


def build_row_time_coverage(dataset):
    properties = {"source": dataset["source"], "id": dataset["id"]}

    tmin, tmax = time_ranges_minmax(dataset.get("time_ranges", []))
    properties["time_min"] = tmin
    properties["time_max"] = tmax

    return properties


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    data_rs = fetch_polars(
        query="source:govdata", build_row=build_row_time_coverage, origins_root="/Bund"
    )
    logging.info(f"Fetched {len(data_rs)} datapoints for polars")
    data_pd = fetch_dataframe(
        query="source:govdata", build_row=build_row_time_coverage, origins_root="/Bund"
    )
    logging.info(f"Fetched {len(data_pd)} datapoints for pandas")
