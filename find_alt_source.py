# import sys
from collections import Counter

from fetch import fetch

lens = Counter()

for dataset in fetch():
    alternatives = dataset.get("alternatives", [])
    lens[len(alternatives)] += 1
    # for alternative in alternatives:
    #      if "Source" in alternative and len(alternatives) > 8:
    #          sys.exit((dataset["source"], dataset["id"]))

for len, cnt in lens.most_common():
    print((len, cnt))
