from pathlib import Path
from collections.abc import Callable

import json
import matplotlib.pyplot as plt
from matplotlib.rcsetup import cycler
from matplotlib import font_manager

from lib.ui_logging import logging

PATH_REPOSITORY = Path("~/data-stories").expanduser()

try:
    with open(PATH_REPOSITORY.joinpath("assets/colors/ui-colors.json"), "rb") as fhd:
        UI_COLORS = json.load(fhd)
except FileNotFoundError:
    logging.error("Could not find file with color definitions")
    raise

FONT_DIRS = [
    PATH_REPOSITORY.joinpath("/assets/fonts")
]  # The path to the custom font file.

COLOR_CYCLE = [
    UI_COLORS["colors primary"]["primary-900"],
    UI_COLORS["colors additional"]["green-900"],
    UI_COLORS["colors additional"]["brown-900"],
    UI_COLORS["colors primary"]["primary-500"],
    UI_COLORS["colors additional"]["green-500"],
    UI_COLORS["colors system messages"]["negative-900"],
]

RC_DEFAULTS = plt.rcParamsDefault

COSTUM_SETTINGS = {}
COSTUM_SETTINGS["axes.edgecolor"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["axes.facecolor"] = UI_COLORS["colors additional"]["white"]
COSTUM_SETTINGS["axes.labelcolor"] = UI_COLORS["color neutral"]["neutral 300"]
COSTUM_SETTINGS["axes.titlecolor"] = "auto"
COSTUM_SETTINGS["axes.labelcolor"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["axes.prop_cycle"] = cycler("color", COLOR_CYCLE)
COSTUM_SETTINGS["figure.edgecolor"] = UI_COLORS["colors additional"]["white"]
COSTUM_SETTINGS["figure.facecolor"] = UI_COLORS["colors additional"]["white"]
COSTUM_SETTINGS["grid.color"] = UI_COLORS["color neutral"]["neutral 100"]
COSTUM_SETTINGS["hatch.color"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["legend.edgecolor"] = UI_COLORS["color neutral"]["neutral 300"]
COSTUM_SETTINGS["legend.facecolor"] = "inherit"
COSTUM_SETTINGS["legend.labelcolor"] = None
COSTUM_SETTINGS["lines.color"] = COLOR_CYCLE[0]
COSTUM_SETTINGS["lines.markeredgecolor"] = "auto"
COSTUM_SETTINGS["lines.markerfacecolor"] = "auto"
COSTUM_SETTINGS["patch.edgecolor"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["patch.facecolor"] = UI_COLORS["colors primary"]["primary-900"]
COSTUM_SETTINGS["patch.force_edgecolor"] = False
COSTUM_SETTINGS["pcolor.shading"] = "auto"
COSTUM_SETTINGS["pcolormesh.snap"] = True
COSTUM_SETTINGS["pdf.inheritcolor"] = False
COSTUM_SETTINGS["savefig.edgecolor"] = "auto"
COSTUM_SETTINGS["savefig.facecolor"] = "auto"
COSTUM_SETTINGS["scatter.edgecolors"] = "face"
COSTUM_SETTINGS["text.color"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["xtick.color"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["xtick.labelcolor"] = "inherit"
COSTUM_SETTINGS["ytick.color"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["ytick.labelcolor"] = "inherit"
COSTUM_SETTINGS["axes3d.xaxis.panecolor"] = (0.95, 0.95, 0.95, 0.5)
COSTUM_SETTINGS["axes3d.yaxis.panecolor"] = (0.9, 0.9, 0.9, 0.5)
COSTUM_SETTINGS["axes3d.zaxis.panecolor"] = UI_COLORS["colors overlay"][
    "overlay background"
]
COSTUM_SETTINGS["boxplot.boxprops.color"] = UI_COLORS["colors overlay"][
    "overlay background"
]
COSTUM_SETTINGS["boxplot.capprops.color"] = UI_COLORS["colors overlay"][
    "overlay background"
]
COSTUM_SETTINGS["boxplot.flierprops.color"] = UI_COLORS["colors additional"]["black"]
COSTUM_SETTINGS["boxplot.flierprops.markeredgecolor"] = UI_COLORS["colors additional"][
    "black"
]
COSTUM_SETTINGS["boxplot.flierprops.markerfacecolor"] = "auto"
COSTUM_SETTINGS["boxplot.meanprops.color"] = COLOR_CYCLE[2]
COSTUM_SETTINGS["boxplot.meanprops.markeredgecolor"] = COLOR_CYCLE[2]
COSTUM_SETTINGS["boxplot.meanprops.markerfacecolor"] = COLOR_CYCLE[2]
COSTUM_SETTINGS["boxplot.medianprops.color"] = COLOR_CYCLE[1]
COSTUM_SETTINGS["boxplot.whiskerprops.color"] = UI_COLORS["colors additional"]["black"]

COSTUM_SETTINGS["font.sans-serif"] = [
    "IBM Plex Sans",
    "DejaVu Sans",
    "Bitstream Vera Sans",
    "Computer Modern Sans Serif",
    "Lucida Grande",
    "Verdana",
    "Geneva",
    "Lucid",
    "Arial",
    "Helvetica",
    "Avant Garde",
    "sans-serif",
]
COSTUM_SETTINGS["font.monospace"] = [
    "IBM Plex Mono",
    "DejaVu Sans Mono",
    "Bitstream Vera Sans Mono",
    "Computer Modern Typewriter",
    "Andale Mono",
    "Nimbus Mono L",
    "Courier New",
    "Courier",
    "Fixed",
    "Terminal",
    "monospace",
]

COLOR_CYCLE_YESNO = [
    UI_COLORS["colors primary"]["primary-900"],
    UI_COLORS["colors primary"]["primary-500"],
    UI_COLORS["colors secondary"]["secondary-800"],
    UI_COLORS["colors secondary"]["secondary-500"],
    UI_COLORS["colors additional"]["green-900"],
    UI_COLORS["colors additional"]["green-500"],
    UI_COLORS["colors system messages"]["negative-900"],
    UI_COLORS["colors system messages"]["negative-500"],
]
YES_NO_PLOT = {}
YES_NO_PLOT["xtick.direction"] = "in"
YES_NO_PLOT["xtick.bottom"] = True
YES_NO_PLOT["xtick.major.width"] = 0.6
YES_NO_PLOT["xtick.major.bottom"] = True
YES_NO_PLOT["xtick.minor.bottom"] = False
YES_NO_PLOT["xtick.top"] = False
YES_NO_PLOT["ytick.left"] = False
YES_NO_PLOT["ytick.right"] = False
YES_NO_PLOT["axes.prop_cycle"] = cycler("color", COLOR_CYCLE_YESNO)
YES_NO_PLOT["axes.grid"] = False
YES_NO_PLOT["axes.spines.bottom"] = True
YES_NO_PLOT["axes.spines.left"] = False
YES_NO_PLOT["axes.spines.right"] = False
YES_NO_PLOT["axes.spines.top"] = False
YES_NO_PLOT["axes.grid"] = False
YES_NO_PLOT["axes.linewidth"] = 0.4

ALTERED_SETTINGS = COSTUM_SETTINGS.copy()
ALTERED_SETTINGS.update(YES_NO_PLOT)


def set_rcparams_yesno():
    plt.rcdefaults()
    plt.rcParams.update(COSTUM_SETTINGS)
    plt.rcParams.update(YES_NO_PLOT)


def load_ui_fonts():
    font_files = font_manager.findSystemFonts(fontpaths=FONT_DIRS)
    for font_file in font_files:
        font_manager.fontManager.addfont(font_file)
        logging.debug(f"Added font {font_file}")


def set_rcparams_ui():
    plt.rcdefaults()
    plt.rcParams.update(COSTUM_SETTINGS)
    logging.debug(f'Setting "axes.edgecolor" {plt.rcParams["axes.edgecolor"]}')
    logging.debug(f'Setting "font.sans-serif" {plt.rcParams["font.sans-serif"]}')


def write_rcparams(prefix: str, filter_func: Callable[[str], bool]):
    with open(PATH_REPOSITORY.joinpath(f"lib/{prefix}.mplstyle"), "w") as fhd:
        for key, val in plt.rcParams.items():
            if key not in ALTERED_SETTINGS:
                continue
            if filter_func(key):
                if isinstance(val, list):
                    fhd.write(f"{key}: {', '.join(val)}\n")
                else:
                    fhd.write(f'{key}: "{val}"\n')
        logging.debug(f"written file {fhd.name}")


def load_sns():
    import seaborn as sns

    plt.style.use("~/data-stories/lib/ui.fonts.mplstyle")
    sns_palette = sns.color_palette(COLOR_CYCLE)
    sns.set_palette(sns_palette)
    sns.set_theme(color_codes=True, rc=COSTUM_SETTINGS)
    return sns


load_ui_fonts()
if __name__ == "__main__":
    logging.getLogger().setLevel("DEBUG")
    set_rcparams_ui()
    write_rcparams("ui.colors", lambda key: "color" in key or "cycle" in key)
    write_rcparams("umwelt.info", lambda key: True)
    write_rcparams("ui.fonts", lambda key: key.startswith("font"))
    set_rcparams_yesno()
    write_rcparams("ui.yesnoplot", lambda key: True)
