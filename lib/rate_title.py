"""Provides wrapper for llamafile server REST interface.

The output is given in json format. This allows to use a partial constraint, i.e one part of the answer is given in a predetermined format, while the other part is given without any prior contraints.

This requires `llama-start-server` to be running on the development VM.
"""

import json
import time

import requests

from lib.logging import logging

GRAMMAR = """

root   ::= "{" ws "\\"rating\\":" ws [1-5] "," "\\"explanation\\":" ws string "}"

string ::=
  "\\"" (
    [^"\\\\\\x7F\\x00-\\x1F] |
    "\\\\" ["\\\\bfnrt] # escapes
  )* "\\"" ws

# Optional space: by convention, applied in this grammar after literal chars when allowed
ws ::= | " " | "\\n" [ \\t]*

"""


def rate_title(title: str):
    title = title.strip()

    # prompt = f"How informative is the following title for a German speaking general audience on a scale from 1 (not informative) to 5 (very informative): {title}"
    prompt = f"Please answer the following question in json format: How informative is the following title for a German speaking general audience on a scale from 1 (not informative) to 5 (very informative): {title}"

    data = {
        "prompt": prompt,
        "temperature": 0,
        "grammar": GRAMMAR,
    }

    start = time.time()
    response = requests.post("http://localhost:8080/completion", data=json.dumps(data))
    end = time.time()

    response.raise_for_status()
    logging.debug("Successfully inquired llamafile API")

    response = response.json()["content"]
    response = json.loads(response.removesuffix("<|eot_id|>"))
    response["eval_time"] = end - start

    return response
