"""Configuration file to provide logging functionality"""

import logging

logging  # Expose module for imports

_formatter = logging.Formatter("%(asctime)s - %(levelname)s - %(name)s - %(message)s")

handler = logging.StreamHandler()
handler.setLevel(logging.DEBUG)
handler.setFormatter(_formatter)

_logger = logging.getLogger()
_logger.setLevel(logging.WARN)
_logger.addHandler(handler)
