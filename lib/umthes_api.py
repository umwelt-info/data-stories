"""This is a wrapper around the UMTHES Api: https://sns.uba.de/de/api
For usage examples, see the main() function."""

import numpy
from urllib.parse import urljoin
import requests
import re
import xml.etree.ElementTree as ET

from lib.ui_logging import logging

SESSION = requests.session()
REGEX_EXTRACT_ID = re.compile(r"_\d{8}")
REGEX_REMOVE_DIRECTIVE = re.compile(r" \[[bB]enutze Unterbegriffe\]")

UMTHES_API = "https://sns.uba.de/umthes/"
UMTHES_API_DE = UMTHES_API + "de/"
ROUTE_FIND = "search.rdf"
ROUTE_CONCEPTS = "concepts"
ROUTE_HIERARCHY = "hierarchy"

NAMESPACES = {
    "dc": "http://purl.org/dc/elements/1.1/",
    "dct": "http://purl.org/dc/terms/",
    "foaf": "http://xmlns.com/foaf/spec/",
    "iqvoc": "http://try.iqvoc.net/schema#",
    "owl": "http://www.w3.org/2002/07/owl#",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs": "http://www.w3.org/2000/01/rdf-schema#",
    "schema": "https://sns.uba.de/umthes/schema#",
    "sdc": "http://sindice.com/vocab/search#",
    "skos": "http://www.w3.org/2004/02/skos/core#",
    "skosxl": "http://www.w3.org/2008/05/skos-xl#",
    "sns": "https://sns.uba.de/schema#",
    "void": "http://rdfs.org/ns/void#",
    "xml": "http://www.w3.org/XML/1998/namespace",
}

try:
    __RESPONSE_STORE
    logging.debug("Reusing response store.")
except NameError:
    __RESPONSE_STORE = {}


def inquire_api(url, **kwargs):
    global SESSION, _RESPONSE_STORE
    request = requests.Request("GET", url, **kwargs)
    request = SESSION.prepare_request(request)
    url = request.url
    try:
        response = __RESPONSE_STORE[url]
    except KeyError:
        response = SESSION.send(request)
        __RESPONSE_STORE[url] = response
        logging.debug("Got response from api")
    return response


def hierarchy_rdf(umthes_id: str, params={"dir": "up"}):
    result = inquire_api(
        UMTHES_API_DE + ROUTE_HIERARCHY + "/" + umthes_id + ".rdf", params=params
    )
    return ET.fromstring(result.text)


def find_rdf(tag, params={"qt": "exact"}):
    params.update({"q": tag})
    url = urljoin(UMTHES_API_DE, ROUTE_FIND)
    result = inquire_api(url, params=params)
    return ET.fromstring(result.text)


def find_ids(tag, params={"qt": "exact"}):
    rdf = find_rdf(tag, params=params)
    resources = collect_ids(rdf)
    if len(resources) == 0:
        return None
    return resources


def find_id(tag, params={"qt": "exact"}):
    ids = find_ids(tag, params=params)
    if ids:
        return ids[0]


def extract_umthes_id(umthes_link: str):
    match_id = REGEX_EXTRACT_ID.search(umthes_link)
    if match_id:
        return match_id.group(0)
    return None


def id_to_resource(umthes_id: str):
    return UMTHES_API + umthes_id


def get_contained_preflabels(root):
    descriptions = root.findall("rdf:Description", NAMESPACES)
    # Extract descriptions with skos:topConceptOf not set
    for description in descriptions:
        if description.find("skos:topConceptOf", NAMESPACES) is None:
            pref_label = description.find("skos:prefLabel", NAMESPACES)
            if pref_label is not None:
                yield pref_label.text


def get_child_concept_names_from_id(umthes_id, params={"dir": "down", "depth": 1}):
    hierarchy = hierarchy_rdf(umthes_id, params=params)
    print(hierarchy.text)
    return get_contained_preflabels(hierarchy)


def get_higher_concepts_from_name(umthes_concept):
    concept_id = find_id(umthes_concept, params={"qt": "contains"})
    if not concept_id:
        logging.error(f"Could not find concept id for {umthes_concept}")
        return None
    return get_higher_concepts_from_id(concept_id)


def get_higher_concepts_from_id(umthes_id):
    hierarchy = hierarchy_rdf(umthes_id)
    return get_contained_preflabels(hierarchy)


def collect_ids(root, selector=".//sdc:link"):
    links = root.findall(selector, NAMESPACES)
    resources = numpy.unique(
        [link.get(f"{{{NAMESPACES['rdf']}}}resource") for link in links]
    )
    return [extract_umthes_id(res) for res in resources]


def get_broader_concepts(umthes_id):
    """Get a list of lists containing hierachical pathes to the UMTHES concept
    denoted by the given umthes_id.

    @params: umthes_id
        A valid UMTHES ID, e.g. "_00014452" for Klimawandel

    @returns:
        [[], []]: list of lists

        each list contains a hierachical path from the given umthes_id upwards.

    """
    hierarchy_et = hierarchy_rdf(umthes_id)
    umthes_resource = id_to_resource(umthes_id)
    return _et_get_broader_concepts(hierarchy_et, umthes_resource)


def _et_get_broader_concepts(root, base_concept_resource):
    """
    This function extracts the information given by the UMTHES Hierarchy API into
    a list of lists upwards from a given base_concept_resource

    @params:
        root: ElementTree
            parsed xml of an UMTHES Hierarchy API reply.
        base_concept_resource: str
            Fully qualified url of an UMTHES concept.
    @returns:
        [[], []]: list of lists

    each list contains a hierachical path from the given base_concept_resource upwards.

    """
    base_concept = root.find(
        f".//rdf:Description[@rdf:about='{base_concept_resource}']",
        namespaces=NAMESPACES,
    )
    if base_concept is None:
        logging.error(f"Could not find base_concept for '{base_concept_resource}'")
        return [[]]
    base_name = base_concept.findall(".//skos:prefLabel[@xml:lang='de']", NAMESPACES)[
        0
    ].text
    logging.debug(f"got base concept {base_name} for {base_concept_resource}")
    broader_pathes = []
    for a_concept in base_concept.findall(".//skos:broader", NAMESPACES):
        a_concept_resource = a_concept.get(f"{{{NAMESPACES['rdf']}}}resource")
        for broader in _et_get_broader_concepts(root, a_concept_resource):
            broader_pathes.append(broader + [base_name])
    if broader_pathes == []:
        logging.debug(f"One endpoint for this concept is {base_name}")
        return [[base_name]]
    return broader_pathes


def strip_use_subconcept_from_name(concept_name: str):
    """Turns  concept named like "Aspekte der Organisation [benutze Unterbegriffe]"
    into "Aspekte der Organisation"

    @params:
        concept_name: str
            any string

    @returns:
        str:
            same as concept_name but with the phrase [benutze Unterbegriffe] removed
    """
    return REGEX_REMOVE_DIRECTIVE.sub("", concept_name)


if __name__ == "__main__":
    """ Exemplary use of the API"""
    logging.getLogger().setLevel(logging.INFO)
    concept = "Klimawandel"
    print("## Find UMTHES ID")
    umthes_id = find_id(concept)
    print(f"{concept} has ID {umthes_id}.")
    print("## Find broader concepts:")
    broader = get_broader_concepts(umthes_id)
    print(
        f"Found the following broader concepts pathes to {concept}:\n {'\n'.join([' -- '.join(v) for v in broader])}"
    )
    print("## Find child concepts:")
    children = list(get_child_concept_names_from_id(umthes_id))
    print(f"direct children of {concept}:\n {children}")
