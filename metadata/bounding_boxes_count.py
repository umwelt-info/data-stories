from fetch import fetch

# Explore the different variants of geoinformations stored in region and bounding_boxes
count_all = 0
counts = {
    "count_no_bounding_boxes_no_region": 0,
    "count_no_bounding_boxes_other_region": 0,
    "count_no_bounding_boxes_geonames": 0,
    "count_bounding_boxes_no_region": 0,
    "count_bounding_boxes_other_region": 0,
    "count_bounding_boxes_geonames": 0,
}

for dataset in fetch(url="https://md.umwelt.info/search"):
    region = dataset["region"]
    bounding_boxes = dataset["bounding_boxes"]

    count_all += 1

    if not bounding_boxes:
        if not region:
            counts["count_no_bounding_boxes_no_region"] += 1
        elif "Other" in region:
            counts["count_no_bounding_boxes_other_region"] += 1
        elif "GeoName" in region:
            counts["count_no_bounding_boxes_geonames"] += 1
    else:
        if not region:
            counts["count_bounding_boxes_no_region"] += 1
        elif "Other" in region:
            counts["count_bounding_boxes_other_region"] += 1
        elif "GeoName" in region:
            counts["count_bounding_boxes_geonames"] += 1

print(f"Total number of harvested datasets: {count_all}\n")

for count in counts:
    print(
        f"Number of variant {count}: {counts[count]} \n It's share of all datasets: {counts[count] / count_all * 100:.2f}%\n"
    )

print(
    f"Total number of datasets with bounding boxes and/or GeoNames: {counts['count_no_bounding_boxes_geonames'] + counts['count_bounding_boxes_geonames'] + counts['count_bounding_boxes_other_region'] + counts['count_bounding_boxes_no_region']}\n As percentage:{(counts['count_no_bounding_boxes_geonames'] + counts['count_bounding_boxes_geonames'] + counts['count_bounding_boxes_other_region'] + counts['count_bounding_boxes_no_region']) / count_all * 100:.2f}%\n"
)
