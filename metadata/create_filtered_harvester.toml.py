#!/bin/env python3
from subprocess import run
import re
from pathlib import Path
import tomllib
import tomli_w
import argparse


def add_databases(config, new_config):
    """Add all non-indexed sources"""
    database_sources = [
        source
        for source in config["sources"]
        if source.get("not_indexed", False) and source not in new_config["sources"]
    ]
    new_config["sources"].extend(database_sources)
    print(
        f"Adding all non-indexed sources \n{'\n'.join([s['name'] for s in database_sources])}"
    )


def one_of_each(config, types):
    """For each type, add the first harvester entry encountered."""
    sources = []
    keys = []
    for source in config["sources"]:
        this_type = source["type"]
        if this_type not in keys:
            if types and this_type not in types:
                continue
            keys.append(source["type"])
            sources.append(source)
            print(f"Adding source {source['name']}")
    return sources


def grepped_source(config, grepper: str):
    """Adding sources if the source code contains the string {grepper}:"""
    print(f"Adding sources if the source code contains the string {grepper}:")
    files = run(
        f"grep -l --no-ignore-case '{grepper}' *.rs",
        shell=True,
        cwd="/home/ubuntu/metadaten/harvester/src",
        capture_output=True,
        text=True,
    )
    print(files.args)
    sources = []
    keys = [file.rsplit(".", 1)[0] for file in files.stdout.splitlines()]
    for source in config["sources"]:
        if source["type"] in keys:
            sources.append(source)
            print(f"Adding source {source['name']}")
    return sources


def sources_affected_in_branch(config):
    """Adding all sources that are changed in the current branch on metadaten rep compared to origin/main"""
    files = run(
        "git diff --name-only origin/main",
        shell=True,
        cwd="/home/ubuntu/metadaten/harvester/src",
        capture_output=True,
        text=True,
    )
    print(
        f"Adding all sources that are changed in the current branch on metadaten rep compared to origin/main by running '{files.args}'"
    )
    sources = []
    is_source = re.compile(r"harvester/src/(.*)\.rs")
    keys = []
    for key in map(is_source.findall, files.stdout.splitlines()):
        if key:
            keys.append(key[0])
    for source in config["sources"]:
        if source["type"] in keys:
            sources.append(source)
            print(f"Adding source {source['name']}")
    return sources


def all_of_these(config, types):
    """Adding all sources of a list of {types}"""
    sources = []
    for source in config["sources"]:
        if source["type"] in types:
            sources.append(source)
            print(f"Adding source {source['name']}")
    return sources


def all_by_name(config, names):
    """Adding all sources of a list of {types}"""
    sources = []
    for source in config["sources"]:
        if source["name"] in names:
            sources.append(source)
            print(f"Adding source {source['name']}")
    return sources


def _remove_duplicates(config):
    sources = []
    names = []
    for source in config["sources"]:
        name = source["name"]
        if name not in names:
            names.append(name)
            sources.append(source)
        else:
            print(f"dropping second occurence of {name}")
    config["sources"] = sources


def _fix_manual_path(config):
    sources = []
    for source in config["sources"]:
        name = source["name"]
        if name == "manual":
            local_path = (
                "file://localhost/home/ubuntu/metadaten/deployment/manual_datasets"
            )
            source["url"] = local_path
            print(f"Setting manual dataset path to {local_path}")
        sources.append(source)
    config["sources"] = sources


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Create a filtered copy of the harvester.toml in /deployment."
    )
    parser.add_argument(dest="names", nargs="*", help="Select by name")
    parser.add_argument("-t", "--type", nargs="+", help="Filter sources by type")
    parser.add_argument(
        "-e",
        "--exemplary",
        nargs="*",
        help="Take first example of each provided type. If none is given, take one of each type found",
    )
    parser.add_argument(
        "-g",
        "--grep",
        nargs=1,
        help="Filter sources containing a specific string in code",
    )
    parser.add_argument(
        "-b",
        "--branch",
        action="store_true",
        help="Filter sources affected in current branch compared to origin/main",
    )
    parser.add_argument(
        "-o",
        "--output",
        help="Output file for the new configuration",
    )
    parser.add_argument(
        "-n",
        "--no_databases",
        action="store_true",
        help="Do not add non_indexed pseudoharvesters",
    )

    args = parser.parse_args()

    try:
        with open("/home/ubuntu/metadaten/deployment/harvester.toml", "rb") as fhd:
            config = tomllib.load(fhd)
    except FileNotFoundError:
        print("No input file found")
        exit(-1)

    new_config = config.copy()
    new_config.pop("sources")
    new_config.update(
        {
            "keep_failed_sources": False,
            "merge_duplicates": True,
            "auto_classify": {"disabled": True},
        }
    )

    if args.type:
        new_config["sources"] = all_of_these(config, args.type)
    elif args.exemplary:
        new_config["sources"] = one_of_each(config, args.exemplary)
    elif args.grep:
        new_config["sources"] = grepped_source(config, args.grep[0])
    elif args.branch:
        new_config["sources"] = sources_affected_in_branch(config)
    else:
        if args.names:
            new_config["sources"] = all_by_name(config, args.names)
        else:
            print("No argument provided, assume --exemplary")
            new_config["sources"] = one_of_each(config, [])

    if not args.no_databases:
        add_databases(config, new_config)
    _remove_duplicates(new_config)
    _fix_manual_path(new_config)

    if args.output:
        outfile = Path(args.output)
    else:
        outfile = Path("~/metadaten/data/harvester.toml").expanduser()

    with open(outfile, "wb") as fhd:
        tomli_w.dump(new_config, fhd)
        print(f"written new config to {outfile}.")
