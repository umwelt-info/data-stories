import sys
import collections

from fetch import fetch


if len(sys.argv) != 2:
    sys.exit("usage: vector_lengths.py <field>")

field = sys.argv[1]

histogram = collections.Counter()

for dataset in fetch():
    length = len(dataset.get(field, []))
    histogram[length] += 1

total = histogram.total()
cumsum = 0

for resources, count in histogram.most_common():
    cumsum += count

    print(f"{resources}: {100 * cumsum / total:.1f} %")
