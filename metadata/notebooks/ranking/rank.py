from urllib.parse import urljoin

import requests
import numpy as np
from sklearn.metrics import ndcg_score

from lib.ui_logging import logging
from fetch import SEARCH_URL_LOCAL

session = requests.Session()


def fetch_with_weights(
    url=SEARCH_URL_LOCAL, query="*", page=1, popularity_weight=1.0, quality_weight=1.0
):
    global session
    logging.debug(f"Inquire {url} for '{query}'")

    params = {
        "query": query,
        "page": page,
        "popularity_weight": popularity_weight,
        "quality_weight": quality_weight,
    }

    headers = {"accept": "application/json"}

    with session.get(
        urljoin(url, "search"), params=params, headers=headers
    ) as response:
        response.raise_for_status()
        response = response.json()

        for dataset in response["results"]:
            id = f"{dataset['source']}/{dataset['id']}"
            title = dataset["title"]
            quality = dataset["quality"]["score"]
            popularity = dataset["popularity"]
            bm25 = (
                dataset["score"]
                / ((1.0 + quality) ** quality_weight)
                / (popularity**popularity_weight)
            )

            yield (id, title, quality, popularity, bm25)


def ndcg_loss(quality_weight, popularity_weight, currentness_score, decay_rate, df):
    ndcg_total = 0.0
    unique_combinations = df[["query", "target group"]].drop_duplicates()

    for _, (query, target_group) in unique_combinations.iterrows():
        query_df = df[(df["query"] == query) & (df["target group"] == target_group)]

        relevant_df = query_df[query_df["ranking"] > 0]
        ids = relevant_df["id"].tolist()

        decay_rate = max(
            decay_rate, 1 / 36500
        )  # Ensure it stays positive to avoid NaNs in the algorithms below
        scores = fetch_scores(
            query, ids, quality_weight, popularity_weight, currentness_score, decay_rate
        )
        pred_scores = np.array(
            [scores.get(id, 0.0) for id in relevant_df["id"]]
        ).reshape(1, -1)

        true_ranks = relevant_df["ranking"].values.reshape(1, -1)
        ndcg_total += ndcg_score(true_ranks, pred_scores)

    return -ndcg_total


def fetch_scores(
    query, ids, quality_weight, popularity_weight, currentness_score, decay_rate
):
    params = {
        "results_per_page": 100,
        "query": query,
        "quality_weight": quality_weight,
        "popularity_weight": popularity_weight,
        "currentness_weight": currentness_score,
        "currentness_rate": decay_rate,
    }

    headers = {"accept": "application/json"}

    resp = session.get("http://localhost:8081/search", params=params, headers=headers)
    resp.raise_for_status()
    results = resp.json()

    scores = {f"{ds['source']}/{ds['id']}": ds["score"] for ds in results["results"]}
    if not scores:
        print(f"Warning: No scores fetched for query '{query}', ids: {ids}")

    return scores


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    for result in fetch_with_weights(
        query="fischotter schleswig-holstein", popularity_weight=0.0
    ):
        print(result)
