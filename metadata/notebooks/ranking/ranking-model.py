import sys

import requests


if __name__ == "__main__":
    if len(sys.argv) != 6:
        sys.exit(
            "usage: <query> <quality_weight> <popularity_weight> <currentness_weight> <currentness_rate>"
        )

    query, quality_weight, popularity_weight, currentness_weight, currentness_rate = (
        sys.argv[1:]
    )

    params = {
        "query": query,
        "quality_weight": quality_weight,
        "popularity_weight": popularity_weight,
        "currentness_weight": currentness_weight,
        "currentness_rate": currentness_rate,
    }

    headers = {"accept": "application/json"}

    response = requests.get(
        "http://localhost:8081/search", params=params, headers=headers
    )
    response.raise_for_status()
    response = response.json()

    for result in response["results"]:
        dates = list(
            map(lambda time_range: time_range["until"], result.get("time_ranges", []))
        )

        issued = result.get("issued")
        if issued:
            dates.append(issued)

        modified = result.get("modified")
        if modified:
            dates.append(modified)

        last_date = max(dates, default=None)

        print(
            f"{result['score']} {last_date} {result['title']} {result['source']}/{result['id']}"
        )
