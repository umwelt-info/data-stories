# This script contains the optimization algorithms for improvement of the ranking
# by adjusting the weights for time

# try:
#     from sklearn.metrics import ndcg_score
#     import numpy as np
#     import cma
#     from pyswarm import pso
#     from openpyxl.utils import get_column_letter
#     from skopt import gp_minimize
#     from skopt.space import Real
# except ModuleNotFoundError:
#     %pip install cma
#     %pip install pyswarm
#     %pip install openpyxl
#     %pip install scikit-optimize
from sklearn.metrics import ndcg_score
import cma
from pyswarm import pso
from skopt import gp_minimize
from skopt.space import Real

import sys
import pandas as pd
import numpy as np
from scipy.optimize import differential_evolution
import requests

from deap import base, creator, tools, algorithms
import random

# run:
# git checkout ranking-model
# cargo xtask indexer
# cargo xtask server

# Import test data set
# the file is located in the folder Ranking \\gruppende\umwelt.info\int\Zusammenarbeit\Ranking
# It was created as "ranking_test_time.xlsx" using metadata/notebooks/ranking_create_ranking_table_landpage.ipynb and then manually filled with human judged rankings ('ranking_test_landing_page_gesamt.xlsx').
df = pd.read_excel("ranking_test_time_all.xlsx")
session = requests.Session()


def fetch_scores(
    query, ids, quality_weight, popularity_weight, currentness_score, decay_rate
):
    params = {
        "results_per_page": 100,
        "query": query,
        "quality_weight": quality_weight,
        "popularity_weight": popularity_weight,
        "currentness_weight": currentness_score,
        "currentness_rate": decay_rate,
    }

    headers = {"accept": "application/json"}

    resp = session.get("http://localhost:8081/search", params=params, headers=headers)
    resp.raise_for_status()
    results = resp.json()

    scores = {f"{ds['source']}/{ds['id']}": ds["score"] for ds in results["results"]}
    if not scores:
        print(f"Warning: No scores fetched for query '{query}', ids: {ids}")

    return scores


def ndcg_loss(quality_weight, popularity_weight, currentness_score, decay_rate, df):
    ndcg_total = 0.0
    unique_combinations = df[["query", "target group"]].drop_duplicates()

    for _, (query, target_group) in unique_combinations.iterrows():
        query_df = df[(df["query"] == query) & (df["target group"] == target_group)]

        relevant_df = query_df[query_df["ranking"] > 0]
        ids = relevant_df["id"].tolist()

        decay_rate = max(
            decay_rate, 1 / 36500
        )  # Ensure it stays positive to avoid NaNs in the algorithms below
        scores = fetch_scores(
            query, ids, quality_weight, popularity_weight, currentness_score, decay_rate
        )
        pred_scores = np.array(
            [scores.get(id, 0.0) for id in relevant_df["id"]]
        ).reshape(1, -1)

        true_ranks = relevant_df["ranking"].values.reshape(1, -1)
        ndcg_total += ndcg_score(true_ranks, pred_scores)

    return -ndcg_total


def cmaes(q, p, c, d, sigma, maxiter):
    # Method A: CMA-ES (Covariance Matrix Adaptation Evolution Strategy)

    # Define the wrapper for the ndcg_loss
    def ndcg_loss_cma(weights, df):
        quality_weight, popularity_weight, currentness_score, decay_rate = weights
        return ndcg_loss(
            quality_weight, popularity_weight, currentness_score, decay_rate, df
        )

    initial_weights = [q, p, c, d]

    # Optimize using CMA-ES
    es = cma.CMAEvolutionStrategy(
        initial_weights,
        sigma,
        {
            "popsize": 5,  # Reduce population size
            "maxiter": maxiter,  # Stop after fewer iterations
            "tolfun": 1e-3,  # Stop if improvements are minimal
            "tolx": 1e-3,
        },
    )
    while not es.stop():
        solutions = es.ask()
        losses = [ndcg_loss_cma(sol, df) for sol in solutions]
        es.tell(solutions, losses)

    opt_weights_a = es.result.xbest
    print("Optimized weights (a, b):", opt_weights_a)


def diff_evol(ql, qh, pl, ph, cl, ch, dl, dh, mutation_strategy, population_size):
    # Method B: Differential Evolution

    # Define the wrapper for the ndcg_loss
    def de_ndcg_loss(weights, df):
        quality_weight, popularity_weight, currentness_score, decay_rate = weights
        return ndcg_loss(
            quality_weight, popularity_weight, currentness_score, decay_rate, df
        )

    # Define bounds for the weights (assuming the weights are between 0 and 1)
    bounds = [(ql, qh), (pl, ph), (cl, ch), (dl, dh)]  # Bounds for weights a, b

    # Run Differential Evolution
    result = differential_evolution(
        de_ndcg_loss,
        bounds,
        args=(df,),
        mutation=mutation_strategy,
        popsize=population_size,
    )
    opt_weights_b = result.x
    print("Optimized weights (a, b):", opt_weights_b)


def bayesian_optimization(ql, qh, pl, ph, cl, ch, dl, dh, n_calls, rs):
    # Method C: Bayesian Optimization
    # Define the search space
    space = [
        Real(ql, qh, name="quality_weight"),
        Real(pl, ph, name="popularity_weight"),
        Real(cl, ch, name="currentness_score"),
        Real(dl, dh, name="decay_rate"),
    ]

    # Bayesian Optimization wrapper
    def bayesian_ndcg_loss(weights):
        return ndcg_loss(*weights, df)

    # Run Bayesian Optimization
    result = gp_minimize(bayesian_ndcg_loss, space, n_calls=n_calls, random_state=rs)

    # Best parameters
    opt_weights_c = result.x
    print("Optimized weights (a, b):", opt_weights_c)


def genetic_algorithm(ql, qh, pl, ph, cl, ch, dl, dh, mutation_rate, crossover_rate):
    # Method D: Genetic algorithm
    # Define the optimization problem
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)

    # Define GA parameters
    def create_individual():
        return [
            random.uniform(ql, qh),  # quality_weight
            random.uniform(pl, ph),  # popularity_weight
            random.uniform(cl, ch),  # currentness_score
            random.uniform(dl, dh),  # decay_rate
        ]

    toolbox = base.Toolbox()
    toolbox.register(
        "individual", tools.initIterate, creator.Individual, create_individual
    )
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", lambda ind: (ndcg_loss(*ind, df),))
    toolbox.register("mate", tools.cxBlend, alpha=crossover_rate)
    toolbox.register("mutate", tools.mutGaussian, mu=0, sigma=mutation_rate, indpb=0.2)
    toolbox.register("select", tools.selTournament, tournsize=3)

    # Run GA
    population = toolbox.population(n=10)
    hof = tools.HallOfFame(1)  # Keep best individual
    algorithms.eaSimple(
        population, toolbox, cxpb=0.5, mutpb=0.2, ngen=10, halloffame=hof, verbose=True
    )

    # Get best individual
    best_ind = tools.selBest(population, k=1)[0]
    opt_weights_d = best_ind
    print("Optimized weights (a, b):", opt_weights_d)


def partso(ql, qh, pl, ph, cl, ch, dl, dh, itermax, inertia):
    # Method E: Particle Swarm Optimization (PSO)

    # Define the wrapper for the ndcg_loss
    def pso_ndcg_loss(weights, df):
        quality_weight, popularity_weight, currentness_score, decay_rate = weights
        return ndcg_loss(
            quality_weight, popularity_weight, currentness_score, decay_rate, df
        )

    # Define bounds for the weights
    lb = [ql, pl, cl, dl]  # Lower bound for weights a, b
    ub = [qh, ph, ch, dh]  # Upper bound for weights a, b

    # Run Particle Swarm Optimization
    opt_weights_e, opt_val = pso(
        pso_ndcg_loss, lb, ub, args=(df,), maxiter=itermax, omega=inertia
    )
    print("Optimized weights (a, b):", opt_weights_e)


if __name__ == "__main__":
    if sys.argv[1] == "cmaes":
        q = float(sys.argv[2])
        p = float(sys.argv[3])
        c = float(sys.argv[4])
        d = float(sys.argv[5])
        sigma = float(sys.argv[6])
        maxiter = int(sys.argv[7])
        cmaes(q, p, c, d, sigma, maxiter)
    elif sys.argv[1] == "de":
        ql = float(sys.argv[2])
        qh = float(sys.argv[3])
        pl = float(sys.argv[4])
        ph = float(sys.argv[5])
        cl = float(sys.argv[6])
        ch = float(sys.argv[7])
        dl = float(sys.argv[8])
        dh = float(sys.argv[9])
        mutation_strategy = float(sys.argv[10])
        population_size = int(sys.argv[11])
        diff_evol(ql, qh, pl, ph, cl, ch, dl, dh, mutation_strategy, population_size)
    elif sys.argv[1] == "bo":
        ql = float(sys.argv[2])
        qh = float(sys.argv[3])
        pl = float(sys.argv[4])
        ph = float(sys.argv[5])
        cl = float(sys.argv[6])
        ch = float(sys.argv[7])
        dl = float(sys.argv[8])
        dh = float(sys.argv[9])
        n_calls = int(sys.argv[10])
        rs = int(sys.argv[11])
        bayesian_optimization(ql, qh, pl, ph, cl, ch, dl, dh, n_calls, rs)
    elif sys.argv[1] == "ga":
        ql = float(sys.argv[2])
        qh = float(sys.argv[3])
        pl = float(sys.argv[4])
        ph = float(sys.argv[5])
        cl = float(sys.argv[6])
        ch = float(sys.argv[7])
        dl = float(sys.argv[8])
        dh = float(sys.argv[9])
        mutation_rate = float(sys.argv[10])
        crossover_rate = float(sys.argv[11])
        genetic_algorithm(ql, qh, pl, ph, cl, ch, dl, dh, mutation_rate, crossover_rate)
    elif sys.argv[1] == "partso":
        ql = float(sys.argv[2])
        qh = float(sys.argv[3])
        pl = float(sys.argv[4])
        ph = float(sys.argv[5])
        cl = float(sys.argv[6])
        ch = float(sys.argv[7])
        dl = float(sys.argv[8])
        dh = float(sys.argv[9])
        maxiter = int(sys.argv[10])
        inertia = float(sys.argv[11])
        partso(ql, qh, pl, ph, cl, ch, dl, dh, maxiter, inertia)
    else:
        sys.exit(f"unknown method:{sys.argv[1]}")
