# This file fetches similar terms from the semantic network service (SNS). An example for usage is given at the end of the file.

import requests
import xml.etree.ElementTree as ET


def fetch_similar_terms(terms, output_file="similar_terms.txt"):
    base_url = "https://sns.uba.de/umthes/de/similar.rdf"
    params = {"terms": terms}
    headers = {"Accept": "application/rdf+xml"}

    response = requests.get(base_url, params=params, headers=headers)

    with open(output_file, "w", encoding="utf-8") as file:
        file.write(f"Request URL: {response.url}\n\n")

        if response.status_code == 200:
            try:
                file.write("Raw RDF/XML Response:\n")
                file.write(response.text + "\n\n")

                # Parse RDF/XML
                root = ET.fromstring(response.content)
                results = []
                for label in root.findall(
                    ".//{http://www.w3.org/2004/02/skos/core#}altLabel"
                ):
                    term = label.text.strip()
                    lang = label.attrib.get(
                        "{http://www.w3.org/XML/1998/namespace}lang", "unknown"
                    )
                    results.append((term, lang))

                file.write("Extracted Results (with language):\n")
                for term, lang in results:
                    file.write(f"Term: {term}, Language: {lang}\n")

                return results
            except ET.ParseError as e:
                file.write(f"Failed to parse RDF/XML: {e}\n")
                return []
        else:
            file.write(
                f"HTTP Error {response.status_code}\nResponse:\n{response.text}\n"
            )
            return []


def main():
    # Example usage
    terms = "Klimawandel"
    similar_terms = fetch_similar_terms(terms)
    print(f"\nSimilar terms for '{terms}': {similar_terms}")


if __name__ == "__main__":
    main()
