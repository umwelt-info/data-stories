import collections
import csv

from fetch import fetch


histogram = collections.Counter()

for dataset in fetch():
    origin = (dataset["source"], dataset["provenance"])
    histogram[origin] += 1

with open("origins.csv", "w", newline="") as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(("source", "provenance", "count"))

    for (source, provenance), count in histogram.items():
        csv_writer.writerow((source, provenance, count))
