import subprocess
import json
from pathlib import Path
from collections import Counter
import tomllib
import tomli_w


def main():
    with open("/home/ubuntu/metadaten/deployment/harvester.toml", "rb") as fhd:
        config = tomllib.load(fhd)
    config["sources"] = [c for c in config["sources"] if c["type"] in ["ckan"]]

    with open("ckan_config.toml", "wb") as fhd:
        tomli_w.dump(config, fhd)

    result_counter = Counter()
    resource_type_counter = Counter()
    resource_format_counter = Counter()
    extras_counter = Counter()
    extras_documentation_counter = Counter()
    extras_theme_counter = Counter()
    extras_spatial_uri_counter = Counter()
    extras_spatial_counter = Counter()

    for source in config["sources"]:
        if source["type"] != "ckan":
            continue
        if source["name"] == "stadt-leipzig":
            continue
        source_ckan = source["name"]
        print(f"Counting elements in {source_ckan}")

        for path in Path(
            f"/home/ubuntu/metadaten/data/responses/{source_ckan}"
        ).iterdir():
            child = subprocess.run(
                ["zstdcat", str(path)], check=True, capture_output=True
            )

            data = json.loads(child.stdout)

            for result in data["result"]["results"]:
                for key, value in result.items():
                    if isinstance(value, (str)):
                        if key not in result_counter:
                            result_counter[key] = Counter()
                        result_counter[key][value] += 1

                for resource in result["resources"]:
                    resource_type = resource.get("mimetype")
                    if resource_type:
                        resource_type_counter[resource_type] += 1

                    resource_format = resource.get("format", [])
                    if resource_format:
                        resource_format_counter[resource_format] += 1

                for extras in result.get("extras", []):
                    extras_key = extras.get("key")
                    extras_value = extras.get("value")
                    if extras_key:
                        extras_counter[extras_key] += 1
                    if extras_key == "documentation":
                        extras_documentation_counter[extras_value] += 1
                    if extras_key == "theme":
                        extras_theme_counter[extras_value] += 1
                    if extras_key == "spatial_uri" and (
                        "dcat" in extras_value or "geonames" in extras_value
                    ):
                        extras_spatial_uri_counter[extras_value] += 1
                    if extras_key == "spatial":
                        extras_spatial_counter[extras_value] += 1

    def print_counter(counter, title, top_n):
        print(f"\n{title}")
        print("| Value | Count |")
        print("| --- | ----- |")
        for key, count in counter.most_common(top_n):
            print(f"| {key} | {count} |")
        print("---------------------------------------\n")

    print("Print CKAN fields with a single value")
    for key, counter in result_counter.items():
        print_counter(counter, key, 15)

    print_counter(extras_counter, "List most common extra fields in CKAN", 25)
    print("Display relevant values for extra fields of interest:")

    print_counter(resource_type_counter, "resource_type", 50)
    print_counter(resource_type_counter, "format", 15)
    print_counter(extras_spatial_uri_counter, "spatial_uri", 25)
    print_counter(extras_spatial_counter, "spatial", 25)


if __name__ == "__main__":
    main()
