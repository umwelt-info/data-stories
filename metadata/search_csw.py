import subprocess
import xml.etree.ElementTree as ET
from pathlib import Path
from collections import Counter
import tomllib
import tomli_w


def main():
    with open("/home/ubuntu/metadaten/deployment/harvester.toml", "rb") as fhd:
        config = tomllib.load(fhd)
    config["sources"] = [
        c for c in config["sources"] if c["type"] in ["csw", "smart_finder"]
    ]

    with open("csw_config.toml", "wb") as fhd:
        tomli_w.dump(config, fhd)

    for source in config["sources"]:
        source_csw = source["name"]
        print(f"Counting source {source_csw}")

        format_name_counter = Counter()
        md_code_counter = Counter()
        rs_codespace_counter = Counter()
        md_anchor_counter = Counter()
        rs_code_counter = Counter()

        for path in Path(
            f"/home/ubuntu/metadaten/data/responses/{source_csw}"
        ).iterdir():
            child = subprocess.run(["zstdcat", path], check=True, capture_output=True)
            try:
                doc = ET.fromstring(child.stdout)
            except ET.ParseError as err:
                print(f"Error: {err}")
                continue

            for el in doc.findall(".//{*}MD_Format//{*}name//{*}CharacterString"):
                format_string = el.text
                if format_string:
                    format_name_counter[format_string] += 1

            for el in doc.findall(".//{*}MD_Identifier//{*}code//{*}CharacterString"):
                code_string = el.text
                if code_string:
                    md_code_counter[code_string] += 1

            for el in doc.findall(
                ".//{*}RS_Identifier//{*}codeSpace//{*}CharacterString"
            ):
                code_space = el.text
                if code_space:
                    rs_codespace_counter[code_space] += 1

            for el in doc.findall(".//{*}RS_Identifier//{*}code//{*}Anchor"):
                rs_identifier = el.text
                if rs_identifier:
                    rs_code_counter[rs_identifier] += 1

            for el in doc.findall(".//{*}MD_Identifier//{*}code//{*}Anchor"):
                anchor = el.text
                if anchor:
                    md_anchor_counter[anchor] += 1

        print(
            f"Counter of already processed geographicDescription fields in {source_csw}: {len(md_code_counter)}"
        )

        def print_counter(counter, title, top_n):
            print(f"\n{title}")
            print("| Value | Count |")
            print("| --- | ----- |")
            for key, count in counter.most_common(top_n):
                print(f"| {key} | {count} |")
            print("---------------------------------------\n")

        print(
            f"Counter RS_Identifier > codeSpace in {source_csw}: {sum(rs_codespace_counter.values())}"
        )
        print(
            f"Counter RS_Identifier > code in {source_csw}: {sum(rs_code_counter.values())}"
        )
        print(
            f"Counter MD_Identfier > anchor in {source_csw}: {sum(md_anchor_counter.values())}\n"
        )

        print_counter(md_anchor_counter, "MD_Identifier > anchor", 15)
        print_counter(rs_codespace_counter, "code_space", 15)
        print_counter(rs_code_counter, "RS_Identifier > code", 15)

        print(f"Description of selective fields in {source_csw}")
        print_counter(format_name_counter, "format", 25)


if __name__ == "__main__":
    main()
