# This is a shiny app creating a polar barplot of the FAIR criteria for the selected source
# First, create_input_data_barplot_FAIR needs be to run to create the preprocessed input data
# use runApp("./metadata/shiny/barplot_FAIR/app.r") to run 

library(shiny)
library(ggplot2)
getwd()
sources <- c("hlnug-he", "umweltatlas-be", "govdata", "thru-de-uba", "website-uba", "umweltchronik-uba",
	"open-umwelt-uba", "ufordat-uba", "flora-web-bfn", "naturdetektive-bfn","website-bfs",	
	"suche-st",	"umweltportal-sh", "korina-ufu", "metadaten-ufz", "stadt-leipzig")
grid_positions <- c(0, 20, 40, 60, 80, 100)
group_colors <- c("#0e59a5", "#aacc00", "#9b5a0e", "#918ae5")

data_list <- list()
for (source_num in sources) {
  data_list[[source_num]] <- list(
    plot_data = readRDS(paste0("plot_data_", source_num, ".rds")),
    base_data = readRDS(paste0("base_data_", source_num, ".rds")),
    source_and_score = readRDS(paste0("source_and_score_", source_num, ".rds"))
  )
}

ui <- fluidPage(
  titlePanel("Barplot Generator by Source"),
  
  sidebarLayout(
    sidebarPanel(
      # Dropdown menu for selecting a source
      selectInput("selected_source", "Wähle eine Quelle:", 
                  choices = sources,  
                  selected = sources[1]) 
    ),
    
    mainPanel(
      plotOutput("barplot", height = "900px", width = "900px")
    )
  )
)


server <- function(input, output) {
  # Render the barplot based on the selected source
  output$barplot <- renderPlot({
    selected_data <- data_list[[input$selected_source]]
    
    # Extract specific datasets
    plot_data <- selected_data$plot_data
    base_data <- selected_data$base_data
    source_and_score <- selected_data$source_and_score
    
    # Generate the barplot
    p <- ggplot(plot_data, aes(x = as.factor(id), y = mean, fill = group)) +
      geom_bar(aes(x = as.factor(id), y = mean, fill = group), stat = "identity", alpha = 0.5) +
      scale_fill_manual(values = group_colors) +
      geom_hline(yintercept = grid_positions, color = "grey", linetype = "solid", linewidth = 0.4) +
      ylim(-100, 120) +
      theme_minimal() +
      theme(
        legend.position = "none",
        axis.text = element_blank(),
        axis.title = element_blank(),
        panel.grid = element_blank(),
        plot.margin = unit(c(0, 0, 0.5, 0), "cm") 
      ) +
      coord_polar(start = -0.1, clip = "off") +
      geom_text(data = plot_data, aes(x = id, y = 110, label = category, hjust = hjust), 
                color = "black", fontface = "bold", alpha = 0.6, size = 4, angle = plot_data$angle, inherit.aes = FALSE) +
      geom_text(data = plot_data, aes(x = id, y = mean / 2, label = round(mean, 0), hjust = hjust), 
                color = "black", fontface = "bold", alpha = 0.6, size = 5, angle = plot_data$angle, inherit.aes = FALSE) +
      ggplot2::annotate("text", x = c(0.37, 0.36, 0.35, 0.34, 0.33), y = c(20, 40, 60, 80, 100), 
               label = c("20", "40", "60", "80", "100"), color = "black", size = 5, angle = 0, fontface = "bold", hjust = 1) +
      ggplot2::annotate("text", x = 0, y = -90, label = source_and_score,
               color = "black", fontface = "bold", size = 6, hjust = 0.5) +
      geom_text(data = base_data, aes(x = c(3.1, 6.1, 10.6, 13.5), y = -13, label = group_score, colour = group), 
                hjust = ifelse(base_data$group %in% c("auffindbar", "zugänglich"), 1, 0), 
                alpha = 0.8, size = 6, fontface = "bold", inherit.aes = FALSE) +

      scale_color_manual(values = group_colors)
    
    print(p)
  })
}

shinyApp(ui = ui, server = server)
