# use the virtual Python environment
# start app with 'streamlit run app.py'
import folium
import streamlit as st

from fetch import fetch_dataframe, SEARCH_URL_REMOTE
from streamlit_folium import st_folium

GERMANY = [51.165691, 10.451526]

source = st.selectbox(
    "Quelle: ",
    [
        "FIS Wasser (Mecklenburg-Vorpommern)",
        "Geoportal Wasser (Saarland)",
        "Grundwasser (Baden-Württemberg)",
        "Grundwassser (Thüringen)",
        "Grundwasser (Bremen)",
        "Grundwasser (Niedersachsen)",
        "Grundwasser (Schleswig-Holstein)",
        "Hochwasservorhersagezentrale (Sachsen-Anhalt)",
        "Messpegel.de",
        "Meeresumweltdatenbank",
        "Niedrigwasserdaten (Bayern)",
        "Niedrigwasserdaten (Baden-Württemberg)",
        "Pegelonline",
        "Sturmflutwarndienst",
        "Undine",
        "Umweltdaten (Hessen)",
        "Umweltprobenbank des Bundes",
        "Wasser.de",
    ],
)

source_dict = {
    "FIS Wasser (Mecklenburg-Vorpommern)": "fis-wasser-mv",
    "Geoportal Wasser (Saarland)": "geoportal-wasser-sl",
    "Grundwasser (Sachsen-Anhalt)": "gld-gw-st",
    "Grundwasser (Baden-Württemberg)": "gw-lu-bw",
    "Grundwassser (Thüringen)": "gw-mst-tlubn-th",
    "Grundwasser (Bremen)": "gw-sukw-hb",
    "Umweltdaten (Hessen)": "hlnug-he",
    "Hochwasservorhersagezentrale (Sachsen-Anhalt)": "hochwasservorhersagezentrale-st",
    "Messpegel.de": "messpegel-de",
    "Meeresumweltdatenbank": "mudab-uba",
    "Niedrigwasserdaten (Bayern)": "nid-by",
    "Niedrigwasserdaten (Baden-Württemberg)": "niz-bw",
    "Grundwasser (Niedersachsen)": "nlwkn-grundwasser-ni",
    "Pegelonline": "pegelonline-gdws",
    "Sturmflutwarndienst": "sturmflutwarndienst-bsh",
    "Grundwasser (Schleswig-Holstein)": "umweltportal-gw-sh",
    "Undine": "undine-bfg",
    "Umweltprobenbank des Bundes": "upb-uba",
    "Wasser.de": "wasser-de-bfg",
}


def make_key(box):
    x0, y0 = [box["min"][k] for k in ["x", "y"]]
    x1, y1 = [box["max"][k] for k in ["x", "y"]]
    return str(x0) + str(y0) + str(x1) + str(y1)


def add_to_map(row):
    # some rows have bounding boxes = nan (eg. mudab-uba)
    if not hasattr(row.bounding_boxes, "__iter__"):
        return

    for box in row.bounding_boxes:
        key = make_key(box)
        if key in known_points:
            continue
        else:
            known_points.add(key)
        x0, y0 = [box["min"][k] for k in ["x", "y"]]
        x1, y1 = [box["max"][k] for k in ["x", "y"]]
        if x0 == x1 and y0 == y1:
            popup = "<a href=" + row.source_url + ">" + row.title + "</a>"
            folium.Marker([y0, x0], popup=popup).add_to(germany_map)
        else:
            continue


data = fetch_dataframe(query=f"source:{source_dict[source]}", url=SEARCH_URL_REMOTE)
germany_map = folium.Map(location=GERMANY, zoom_start=6)
known_points = set()

if "bounding_boxes" not in data.columns:
    print("No bounding boxes in data")
else:
    data.apply(add_to_map, axis=1)

# call to render Folium map in Streamlit
st_data = st_folium(germany_map, width=725)
