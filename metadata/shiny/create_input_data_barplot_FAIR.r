


library(jsonlite);packageVersion("jsonlite")
library(tidyverse);packageVersion("tidyverse")

sampling_fraction <- 0.2
url <- sprintf("https://md.umwelt.info/search/all?query=*&sampling_fraction=%s", sampling_fraction)
dataframe <- stream_in(url(url), pagesize = 10000)



#### Extract quality data
sources <- c("hlnug-he", "umweltatlas-be", "govdata", "thru-de-uba", "website-uba", "umweltchronik-uba",
	"open-umwelt-uba", "ufordat-uba", "flora-web-bfn", "naturdetektive-bfn", "website-bfs",	
	"suche-st",	"umweltportal-sh", "korina-ufu", "metadaten-ufz", "stadt-leipzig")
 
sources_list <- vector("list", length(sources))
names(sources_list) <- sources
for (source_num in sources) {


    # source_num <- 8 # Change to show the plot for the respective source

    data <-
        dataframe |> 
        as_tibble() |>
        filter(source == source_num) |>
        select(quality) |>
        unnest_wider(quality) |>
        unnest(findability, names_sep = "_") |>
        unnest(accessibility, names_sep = "_") |>
        unnest(interoperability, names_sep = "_") |>
        unnest(reusability, names_sep = "_")

    data <- data |>
    select(-c("findability_spatial", "findability_identifier", "accessibility_landing_page", "reusability_license"))



    #### Preprocess data for the plot creation

    data_numeric <- data %>%
        mutate(across(where(is.logical), \(x) as.numeric(x)))

    translations <- c(
    "title" = "Titel",
    "description" = "Beschreibung",
    "keywords" = "Schlagwörter",
    "spatial_score"= "Raumbezug",
    "temporal" = "Zeitbezug",
    "direct_access"=  "Direktzugriff" ,
    "publicly_accessible"=  "Öffentlich",
    "machine_readable_data" = "Maschinenlesbare Daten",
    "machine_readable_metadata" = "Maschinenlesbare Metadaten",
    "media_type" = "Medientyp",
    "open_file_format" = "Offenes Dateiformat" ,
    "license_score" = "Lizenz" ,
    "contact_info" = "Kontakt" ,
    "publisher_info"= "Veröffentlicher",
    "Findability" = "auffindbar",
    "Accessibility" = "zugänglich",
    "Interoperability" = "interoperabel", 
    "Reusability" =  "wiederverwendbar",
    "landing_page_score" = "Direktlink"
    )

    group_scores <- data_numeric %>%
    summarize(across(where(is.numeric), function(x) mean(x, na.rm = TRUE))) %>%
    pivot_longer(
        cols = everything(),
        names_to = "category",
        values_to = "mean"
    ) %>%
    filter(category %in% c("findability_score", "accessibility_score", "interoperability_score", "reusability_score")) %>% 
    mutate(
        mean = mean * 100, # Scale the mean values to 0–100
        group = case_when(
        str_detect(category, "findability") ~ "Findability",
        str_detect(category, "accessibility") ~ "Accessibility",
        str_detect(category, "interoperability") ~ "Interoperability",
        str_detect(category, "reusability") ~ "Reusability",
        TRUE ~ "Other"
        ),
        group = str_replace_all(group, translations),
    ) %>%
    select(group, group_mean = mean) 

    plot_data <- data_numeric %>%
    summarize(across(where(is.numeric), function(x) mean(x, na.rm = TRUE))) %>%
    pivot_longer(
        cols = everything(),
        names_to = "category",
        values_to = "mean"
    ) %>%
    filter(!category %in% c("findability_score", "accessibility_score", "interoperability_score", "reusability_score", "score")) %>%
    mutate(
        mean = mean * 100, # Scale the mean values to 0–100
        group = case_when(
        str_detect(category, "findability") ~ "Findability",
        str_detect(category, "accessibility") ~ "Accessibility",
        str_detect(category, "interoperability") ~ "Interoperability",
        str_detect(category, "reusability") ~ "Reusability",
        TRUE ~ "Other"
        ),
        id = row_number(),
        source_name = source_num,
        category = case_when(
        category %in% c("findability_score", "accessibility_score", "interoperability_score", "reusability_score") ~ category,
        TRUE ~ str_remove(category, "^(findability|accessibility|interoperability|reusability)_")
        ),
        category = str_replace_all(category, translations),
        group = str_replace_all(group, translations),
        category = str_replace_all(category, " ", "\n"),
        category = str_to_title(category)
    ) %>%
    left_join(group_scores, by = "group") # # Join the group scores back to the main table

    score_overall <- mean(data_numeric$score)*100

    # print(plot_data)



    #### Prepare plot creation
    empty_bar <- 3
    factor_levels <- c("auffindbar", "zugänglich", "interoperabel", "wiederverwendbar")
    plot_data$group <- factor(plot_data$group, levels = factor_levels)
    plot_data <- plot_data %>%
        arrange(group) %>%
        mutate(id = seq(1, n()))
    
    # Get the name and the y position of each label
    number_of_bar <- nrow(plot_data)
    angle <- 90 - 360 * (plot_data$id-0.5) /number_of_bar  # substract 0.5 because the letter must have the angle of the center of the bars. Not extreme right(1) or extreme left (0)
    plot_data$hjust <- ifelse(angle < -90, 1, 0)
    plot_data$angle <- ifelse(angle < -90, angle+180, angle)
    
    # prepare a data frame for base lines
    base_data <- plot_data %>% 
    group_by(group, group_mean) %>% 
    summarize(start=min(id), end=max(id) - empty_bar) %>% 
    rowwise() %>% 
    mutate(title=mean(c(start, end))) %>%
    mutate(group_score = paste0(group, "\n", round(group_mean, 0), "%"))
    
    source_and_score <- paste0(plot_data$source_name, "\n", round(score_overall, 0), "%")

    saveRDS(plot_data, paste0("./metadata/shiny/barplot_FAIR/plot_data_", source_num, ".rds"))
    saveRDS(base_data, paste0("./metadata/shiny/barplot_FAIR/base_data_", source_num, ".rds"))
    saveRDS(source_and_score, paste0("./metadata/shiny/barplot_FAIR/source_and_score_", source_num, ".rds"))
} 