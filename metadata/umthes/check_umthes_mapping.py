#!/usr/bin/env python3

import tomllib
from pathlib import Path
import pandas
import logging
import csv


from lib.umthes_api import (
    find_id as find_umthes_id,
    get_child_concept_names_from_id,
    get_higher_concepts_from_name,
)

MAPPING_PATH_SPLITTER = "/"

WORKSPACE_PATH = Path("/home/ubuntu/metadaten/")
tag_mappings_file = WORKSPACE_PATH / "src" / "dataset" / "top_level_facets.toml"

if WORKSPACE_PATH is None:
    raise Exception("failed to determine workspace path")


def check_subconcepts_for_orphans(top_concept: str, umthes_sub_concepts: set):
    valid_subconcepts = set(get_child_concept_names_from_id(top_concept))
    return umthes_sub_concepts.difference(valid_subconcepts)


def check_multilevel_entries(top_concept: str, next_levels: str):
    top_concep_id = find_umthes_id(top_concept, params={"qt": "contains"})
    if not top_concep_id:
        print(f"Could not find Umthes ID for {top_concept}")
        return None
    child_concepts = set(get_child_concept_names_from_id(top_concep_id))
    match next_levels.split(MAPPING_PATH_SPLITTER, 1):
        case (second_level, next_levels):
            if second_level not in child_concepts:
                return second_level
            else:
                return check_multilevel_entries(second_level, next_levels)
        case [second_level]:
            if second_level not in child_concepts:
                return second_level
            else:
                return None


def get_higher_concept_list(concept):
    try:
        return list(get_higher_concepts_from_name(concept))
    except TypeError:
        return []


if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    with open(tag_mappings_file, "rb") as fhd:
        print(f"Read mappings from {tag_mappings_file}")
        tag_mappings = tomllib.load(fhd)

    orphaned_concepts = {}
    for top_level, second_levels in tag_mappings.items():
        first_level_concept_id = find_umthes_id(top_level, params={"qt": "contains"})
        if not first_level_concept_id:
            orphaned_concepts[top_level] = []
            print(f"Could not find UMTHES ID for {top_level}")
            continue
        # print(first_level_concept_id)

        orphaned = check_subconcepts_for_orphans(
            first_level_concept_id, set(second_levels.keys())
        )
        if orphaned:
            orphaned_concepts[top_level] = list(orphaned)

        # print(dependends)
        for second_level, mapping in second_levels.items():
            might_be_orphaned = check_multilevel_entries(top_level, second_level)
            if might_be_orphaned:
                print(might_be_orphaned)
                orphaned_concepts[top_level + "_" + second_level] = might_be_orphaned

    data = pandas.Series(orphaned_concepts, name="Concept").explode().to_frame()
    emtpy_top_levels = data.query("Concept == ''")
    data = data.query("Concept != ''")

    data["Higher_proposal"] = data.Concept.map(get_higher_concept_list)
    data.to_csv("orphaned_concepts_and_new_places.csv")
    emtpy_top_levels.to_csv("orphaned_top_level_concepts.csv")

    umthes_pathes = []
    all_concepts = set()
    for top_level, second_levels in tag_mappings.items():
        umthes_pathes += [top_level]
        all_concepts.add(top_level)
        for second in second_levels:
            umthes_pathes += [top_level + "/" + second]
            concept_name = second.rsplit("/")[-1]
            all_concepts.add(concept_name)

    endpunkte_kategorien = dict()
    for umthes_path in umthes_pathes:
        concept_name = umthes_path.rsplit("/")[-1]
        print(concept_name)
        try:
            concept_id = find_umthes_id(concept_name)
        except KeyError:
            print(f"Got no ID for {concept_name}")
            continue
        if not concept_id:
            print(f"Got no ID for {concept_name}")
            continue
        for sub_category in get_child_concept_names_from_id(concept_id):
            if sub_category == concept_name:
                continue
            if sub_category in all_concepts:
                continue
            else:
                if sub_category in endpunkte_kategorien:
                    endpunkte_kategorien[sub_category] += [umthes_path]
                else:
                    endpunkte_kategorien[sub_category] = [umthes_path]

    for category, pathes in endpunkte_kategorien.items():
        if len(pathes) > 1:
            print(f"Problemfall: {category} found in \n\t{'\n\t'.join(pathes)}")

    csv_data = []
    for category, pathes in endpunkte_kategorien.items():
        if len(pathes) > 1:
            row = [category]
            row.extend(pathes)
            csv_data.append(row)
    with open("umthes_categories.csv", mode="w", newline="", encoding="utf-8") as file:
        writer = csv.writer(file)
        writer.writerow(["Category", "Path1", "Path2", "Path3"])
        writer.writerows(csv_data)
