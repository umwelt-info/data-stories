import requests
import pandas as pd
from datetime import datetime
import os
from pathlib import Path

from lib.ui_logging import logging
from fetch import fetch_dataframe, time_ranges_minmax

NOW = datetime.now().strftime("%Y-%m-%dT%H:%M:00")

SCOPUS_API_KEY = os.getenv("SCOPUS_API_KEY")
if not SCOPUS_API_KEY:
    raise EnvironmentError("Please set 'SCOPUS_API_KEY' in the environment")

SCOPUS_BASE_URL = "https://api.elsevier.com/content/search/scopus"
SCOPUS_QUERY = (
    "{umwelt.info} OR {Nationales Zentrum für Umwelt- und Naturschutzinformationen}"
)

OPENALEX_BASE_URL = "https://api.openalex.org/works"
OPENALEX_QUERY = (
    '("umwelt.info" OR "Nationales Zentrum für Umwelt- und Naturschutzinformationen")'
)

CSV_FILE = Path("publication_counts.csv")
CSV_UMWELTINFO = Path("umweltinfo.counts.csv")
CSV_ORIGINS = Path("umweltinfo.origins.counts.csv")


def query_scopus(query, api_key, start=0):
    params = {
        "query": query,
        "apiKey": api_key,
        "start": start,
    }
    response = requests.get(SCOPUS_BASE_URL, params=params)
    response.raise_for_status()
    return response.json()


def get_scopus_publication_count(query, api_key):
    response = query_scopus(query, api_key)
    try:
        return int(response["search-results"]["opensearch:totalResults"])
    except KeyError:
        raise ValueError("Unexpected response format from Scopus API.")


def get_openalex_publication_count(query):
    response = query_openalex(query)
    if "meta" in response and "count" in response["meta"]:
        return int(response["meta"]["count"])
    else:
        raise ValueError("Unexpected response format from OpenAlex API.")


def update_publication_count_csv(
    csv_file, scopus_query, scopus_api_key, openalex_query
):
    try:
        scopus_count = get_scopus_publication_count(scopus_query, scopus_api_key)
    except requests.RequestException as e:
        raise RuntimeError("Scopus API request failed") from e

    try:
        openalex_count = get_openalex_publication_count(openalex_query)
    except requests.RequestException as e:
        print(f"OpenAlex API request failed: {e}")
        raise

    try:
        data = pd.read_csv(csv_file, index_col=0)
    except FileNotFoundError:
        logging.warning(f"Did not find {csv_file}, creating new one")
        data = pd.DataFrame(columns=["API"])
        data.set_index(["API"], inplace=True)

    data.loc["scopus", NOW] = scopus_count
    data.loc["openalex", NOW] = openalex_count

    data.to_csv(csv_file, index=True)
    logging.info(
        f"Updated {csv_file} with {scopus_count} (Scopus) and {openalex_count} (OpenAlex) publications."
    )


def get_umweltinfo():
    """Inquire the umwelt.info API to get info about umwelt.info that is on our metadata index"""

    def build_row(dataset):
        properties = {
            "source": dataset["source"],
            "id": dataset["id"],
            "title": dataset["title"],
            "types": dataset.get("types"),
            "origins": dataset["origins"],
            "global_identifier": dataset.get("global_identifier"),
        }

        tmin, tmax = time_ranges_minmax(dataset.get("time_ranges", []))
        properties["time_min"] = tmin
        properties["time_max"] = tmax

        return properties

    data = pd.DataFrame()

    for query in [
        '"umwelt.info"',
        '"Umweltinformationszentrums in Merseburg"',
        '"Nationalen Zentrums für Umwelt- und Naturschutzinformationen"',
    ]:
        data_term = fetch_dataframe(query=query, build_row=build_row)
        logging.info(f"Got {len(data_term)} datasets for query {query}")
        data = pd.concat((data, data_term))

    data_origins = fetch_dataframe(
        query="*", build_row=build_row, origins_root="/Bund/UBA/umwelt.info"
    )
    logging.info(f"Got {len(data_origins)} datasets for origin umwelt.info")
    data = pd.concat((data, data_origins))
    data.reset_index(drop=False, inplace=True)
    data.drop_duplicates(subset=["source", "id"], inplace=True)
    total_unique = len(data)
    data = data.explode(column="types", ignore_index=True)
    data = data.explode(column="origins", ignore_index=True)
    data["types"] = (
        pd.json_normalize(data.types)
        .fillna("NoType")
        .apply(lambda row: "_".join(map(str, row)), axis=1)
    )
    data["global_identifier"] = (
        pd.json_normalize(data.global_identifier)
        .fillna("")
        .apply(lambda row: "_".join(map(repr, row)), axis=1)
    )
    logging.info(f"Got {len(data)} datasets for after deduplication")
    return data, total_unique


def update_umweltinfo_count():
    """Tries to read a pre-existing CSV file containing the result of
    previous runs and adds a column named by the current date with
    current values of mentions on umwelt.info in our index.
    The counts are inferred using the get_umweltinfo() function.
    """
    total_row = ("Total", "Total")
    try:
        data = pd.read_csv(CSV_UMWELTINFO, index_col=False)
        data.set_index(["types", "origins"], inplace=True, drop=True)
    except FileNotFoundError:
        logging.warning(f"Did not find {CSV_UMWELTINFO}, creating new one")
        data = pd.DataFrame(columns=["types", "origins"])
        data.set_index(["types", "origins"], inplace=True)

    new_df, new_total = get_umweltinfo()

    new_data = new_df.groupby(["types", "origins"]).id.nunique()
    new_data.loc[("Total", "Total")] = new_total
    new_data.name = NOW
    new_data = new_data.to_frame()

    data = pd.merge(data, new_data, left_index=True, right_index=True, how="outer")

    # I really want the total to be the last line:
    data["tmp"] = data.reset_index().index.values
    data.loc[total_row, "tmp"] = len(data) + 1
    data.sort_values(by="tmp", inplace=True)
    data.drop("tmp", axis=1, inplace=True)

    data.reset_index(drop=False).to_csv(CSV_UMWELTINFO, index=False)

    logging.info(f"written to  {CSV_UMWELTINFO}")
    return data


def get_origins_umweltinfo():
    """Get origins top level and license open/close type for all dataset on umwelt.info"""

    def build_row(dataset):
        properties = {
            "source": dataset["source"],
            "id": dataset["id"],
            "origins": dataset["origins"],
            "license": dataset.get("license"),
        }

        return properties

    data_all = fetch_dataframe(query="*", build_row=build_row)
    total_len = len(data_all)
    data_all["license_type"] = (
        data_all.license.map(lambda d: d["path"]).str.split("/").str.get(1)
    )
    data = data_all.explode(column="origins", ignore_index=True)
    data["origin_type"] = data.origins.str.split("/").str.get(1)
    return data, total_len


def update_origins_count():
    """Tries to read a pre-existing CSV file containing the result of
    previous runs and adds a column named by the current date with
    current values of license types per top-level origin in our index.
    The counts are inferred using the get_origins_umweltinfo() function.
    """
    index_columns = ["origin_type", "license_type"]
    total_row = ("Total", "Total")
    try:
        data = pd.read_csv(CSV_ORIGINS, index_col=False)
        data.set_index(index_columns, inplace=True, drop=True)
    except FileNotFoundError:
        logging.warning(f"Did not find {CSV_ORIGINS}, creating new one")
        data = pd.DataFrame(columns=index_columns)
        data.set_index(index_columns, inplace=True)

    new_df, new_total = get_origins_umweltinfo()

    new_data = new_df.groupby(index_columns).origins.count()
    new_data.loc[total_row] = new_total
    new_data.name = NOW
    new_data = new_data.to_frame()

    data = pd.merge(data, new_data, left_index=True, right_index=True, how="outer")

    # I really want the total to be the last line:
    data["tmp"] = data.reset_index().index.values
    data.loc[total_row, "tmp"] = len(data) + 1
    data.sort_values(by="tmp", inplace=True)
    data.drop("tmp", axis=1, inplace=True)

    data.reset_index(drop=False).to_csv(CSV_ORIGINS, index=False)

    logging.info(f"written to {CSV_ORIGINS}")
    return data


def query_openalex(query):
    params = {"search": query}
    response = requests.get(OPENALEX_BASE_URL, params=params)
    response.raise_for_status()
    return response.json()


if __name__ == "__main__":
    logging.getLogger().setLevel("DEBUG")
    update_publication_count_csv(CSV_FILE, SCOPUS_QUERY, SCOPUS_API_KEY, OPENALEX_QUERY)
    data = update_umweltinfo_count()
    origins = update_origins_count()
